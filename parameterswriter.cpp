#include "parameterswriter.h"

bool ParametersWriter::writePolyLine(const std::string &fileName, std::vector<std::pair<double, double> > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto node: parameter)
    {
        outFile << node.first << "\t" << node.second << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writePolyLine(const std::string &fileName, std::vector<std::pair<int, double> > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto node: parameter)
    {
        outFile << node.first << "\t" << node.second << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeScalar(const std::string &fileName, std::vector<std::vector<double> > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto curve: parameter)
    {
        for (auto node: curve)
        {
            //            if (!std::isnan(node) && !std::isinf(node))
            outFile << node << "\t";
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeScalar(const std::string &fileName, std::vector<std::vector<int> > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto curve: parameter)
    {
        for (auto node: curve)
        {
            //            if (!std::isnan(node) && !std::isinf(node))
            outFile << node << "\t";
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeVector(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto curve: parameter)
    {
        for (auto node: curve)
        {

            outFile << node.first << "," << node.second << "\t";
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeTensor(const std::string &fileName, std::vector<std::vector<std::vector<double> > > parameter)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }
    // Пишем данные в цикле
    for (auto curve: parameter)
    {
        for (auto node: curve)
        {
            if (node.size()==3)
                outFile << node[0] << "," << node[1] << ","  << node[2] << "\t";
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeColormapScalar(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<double> > parameter, ParametersWriter::DataType dataType)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }

    if (mesh.size() != parameter.size())
    {
        outFile.close();
        return false;
    }

    // Пишем данные в цикле
    for (int i=0; i<(int)mesh.size(); ++i)
    {
        if (mesh[i].size() != parameter[i].size())
        {
            outFile.close();
            return false;
        }
        for (int j=0; j<(int)mesh[i].size(); ++j)
        {
            double value = parameter[i][j];
            switch (dataType) {
            case ParametersWriter::temp:
                value = parameter[i][j] - 273;
                break;
            case ParametersWriter::press:
                value = parameter[i][j]/1e9;
                break;
            default:
                break;
            }
            outFile << "\n" << mesh[i][j].first/1e3 << "\t" << mesh[i][j].second/1e3 << "\t"  << value;
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeColormapScalar(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<int> > parameter, ParametersWriter::DataType dataType)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }

    if (mesh.size() != parameter.size())
    {
        outFile.close();
        return false;
    }

    // Пишем данные в цикле
    for (int i=0; i<(int)mesh.size(); ++i)
    {
        if (mesh[i].size() != parameter[i].size())
        {
            outFile.close();
            return false;
        }
        for (int j=0; j<(int)mesh[i].size(); ++j)
        {
            double value = parameter[i][j];
            outFile << "\n" << mesh[i][j].first/1e3 << "\t" << mesh[i][j].second/1e3 << "\t"  << value;
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}

bool ParametersWriter::writeColormapVector(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<std::pair<double, double> > > parameter, ParametersWriter::DataType dataType)
{
    std::ofstream outFile (fileName);
    if (!outFile.is_open())
    {
        outFile.close();
        return false;
    }

    if (mesh.size() != parameter.size())
    {
        outFile.close();
        return false;
    }

    // Пишем данные в цикле
    for (int i=0; i<(int)mesh.size(); ++i)
    {
        if (mesh[i].size() != parameter[i].size())
        {
            outFile.close();
            return false;
        }
        for (int j=0; j<(int)mesh[i].size(); ++j)
        {
            double valueX = parameter[i][j].first;
            double valueZ = parameter[i][j].second;
            switch (dataType) {
            default:
                break;
            }
            outFile << "\n" << mesh[i][j].first/1e3 << "\t" << mesh[i][j].second/1e3 << "\t"  << valueX << "\t" << valueZ;
        }
        outFile << "\n";
    }
    outFile.close();
    return true;
}
