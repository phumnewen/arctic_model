#include "modelwriter.h"

int ModelWriter::writeModel(std::shared_ptr<Model> model,  std::string note)
{
    std::time_t timeNow = std::time(nullptr);
    char timeStr[100];
    std::strftime(timeStr, sizeof(timeStr), "%Y-%m-%d_%H:%M:%S", std::gmtime(&timeNow));
    std::string UTCTimeEnd (timeStr);

    int writtenErrors = 0;

//    if (std::strftime(timeStr, sizeof(timeStr), "%Y-%m-%d_%H:%M:%S", std::gmtime(&timeNow)))
//    {
        if (!ParametersWriter::writeVector("../output/"+note+"_mesh_"+UTCTimeEnd+".csv", model->_mesh)) writtenErrors += 1;
        if (!ParametersWriter::writeVector("../output/"+note+"_cells_"+UTCTimeEnd+".csv", model->_cells)) writtenErrors += 2;
        if (!ParametersWriter::writeScalar("../output/"+note+"_layers_"+UTCTimeEnd+".csv", model->_cellLayerIndexes)) writtenErrors += 4;
        if (!ParametersWriter::writeVector("../output/"+note+"_force_"+UTCTimeEnd+".csv", model->_force)) writtenErrors += 8;
        if (!ParametersWriter::writeVector("../output/"+note+"_displacement_"+UTCTimeEnd+".csv", model->_displacement)) writtenErrors += 16;
        if (!ParametersWriter::writeScalar("../output/"+note+"_temperature_"+UTCTimeEnd+".csv", model->_T)) writtenErrors += 32;
        if (!ParametersWriter::writeScalar("../output/"+note+"_pressure_"+UTCTimeEnd+".csv", model->_P)) writtenErrors += 64;
        if (!ParametersWriter::writeColormapScalar("../output/"+note+"_layersCM_"+UTCTimeEnd+".csv", model->_cells, model->_cellLayerIndexes, ParametersWriter::layers)) writtenErrors += 6400;
        if (!ParametersWriter::writeColormapScalar("../output/"+note+"_pressureCM_"+UTCTimeEnd+".csv", model->_mesh, model->_P, ParametersWriter::press)) writtenErrors += 6400;
        if (!ParametersWriter::writeColormapScalar("../output/"+note+"_temperatureCM_"+UTCTimeEnd+".csv", model->_mesh, model->_T, ParametersWriter::temp)) writtenErrors += 6400;
        if (!ParametersWriter::writeColormapScalar("../output/"+note+"_densityCM_"+UTCTimeEnd+".csv", model->_cells, model->_dens, ParametersWriter::dens)) writtenErrors += 6400;
        if (!ParametersWriter::writeColormapVector("../output/"+note+"_forceCM_"+UTCTimeEnd+".csv", model->_mesh, model->_force, ParametersWriter::force)) writtenErrors += 6400;
        if (!ParametersWriter::writeScalar("../output/"+note+"_density_"+UTCTimeEnd+".csv", model->_dens)) writtenErrors += 128;
        if (!ParametersWriter::writeTensor("../output/"+note+"_stress_"+UTCTimeEnd+".csv", stressInCells(model))) writtenErrors += 256;
        if (!ParametersWriter::writePolyLine("../output/"+note+"_surfLine_"+UTCTimeEnd+".csv", boundaryLineMovement(model, ModelWriter::srf))) writtenErrors += 512;
        if (!ParametersWriter::writePolyLine("../output/"+note+"_labLine_"+UTCTimeEnd+".csv", boundaryLineMovement(model, ModelWriter::lab))) writtenErrors += 1024;
        if (!ParametersWriter::writePolyLine("../output/"+note+"_midLine_"+UTCTimeEnd+".csv", getLithMidLine(model))) writtenErrors += 2048;

//        if (!ParametersWriter::writePolyLine("../output/surfLine_"+UTCTimeEnd+".csv", boundaryLine(model, ModelWriter::srf))) writtenErrors += 512;
//        if (!ParametersWriter::writePolyLine("../output/labLine_"+UTCTimeEnd+".csv", boundaryLine(model, ModelWriter::lab))) writtenErrors += 1024;
//        if (!ParametersWriter::writePolyLine("../output/maxDispl_"+UTCTimeEnd+".csv", model->maxDisplacement)) writtenErrors += 2048;
//    }
//    else
//    {
//        if (!ParametersWriter::writeVector("../output/mesh_20-05.csv", model->_mesh)) writtenErrors += 1;
//        if (!ParametersWriter::writeVector("../output/cells_20-05.csv", model->_cells)) writtenErrors += 2;
//        if (!ParametersWriter::writeScalar("../output/layers_20-05.csv", model->_cellLayerIndexes)) writtenErrors += 4;
//        if (!ParametersWriter::writeVector("../output/force_20-05.csv", model->_force)) writtenErrors += 8;
//        if (!ParametersWriter::writeVector("../output/displacement_20-05.csv", model->_displacement)) writtenErrors += 16;
//        if (!ParametersWriter::writeScalar("../output/temperature_20-05.csv", model->_T)) writtenErrors += 32;
//        if (!ParametersWriter::writeScalar("../output/pressure_20-05.csv", model->_P)) writtenErrors += 64;
//        if (!ParametersWriter::writeScalar("../output/density_20-05.csv", model->_dens)) writtenErrors += 128;
//        if (!ParametersWriter::writeTensor("../output/stress_20-05.csv", stressInCells(model))) writtenErrors += 256;
//        if (!ParametersWriter::writePolyLine("../output/surfLine_20-05.csv", boundaryLineMovement(model, ModelWriter::srf))) writtenErrors += 512;
//        if (!ParametersWriter::writePolyLine("../output/labLine_20-05.csv", boundaryLineMovement(model, ModelWriter::lab))) writtenErrors += 1024;
//        if (!ParametersWriter::writePolyLine("../output/midLine_20-05.csv", getLithMidLine(model))) writtenErrors += 2048;
////        if (!ParametersWriter::writePolyLine("../output/surfLine_20-05.csv", boundaryLine(model, ModelWriter::srf))) writtenErrors += 512;
////        if (!ParametersWriter::writePolyLine("../output/labLine_20-05.csv", boundaryLine(model, ModelWriter::lab))) writtenErrors += 1024;
////        if (!ParametersWriter::writePolyLine("../output/maxDispl_20-05.csv", model->maxDisplacement)) writtenErrors += 2048;
//    }
    return writtenErrors;
}

std::vector<std::vector<std::vector<double> > > ModelWriter::stressInCells(std::shared_ptr<Model> model)
{
    // размерность - как у вектора ячеек модели
    std::vector<std::vector<std::vector<double>>> outStress;

    for (auto subvct: model->_stress)
    {
        std::vector<std::vector<double>> outSub;
        for (auto node: subvct)
        {
            std::vector<double> outNode;
            double sXX = (node[Model::T_0][Model::xx] + node[Model::T_1][Model::xx] + node[Model::T_2][Model::xx] + node[Model::T_3][Model::xx]) / 4;
            double sZZ = (node[Model::T_0][Model::zz] + node[Model::T_1][Model::zz] + node[Model::T_2][Model::zz] + node[Model::T_3][Model::zz]) / 4;
            double sXZ = (node[Model::T_0][Model::xz] + node[Model::T_1][Model::xz] + node[Model::T_2][Model::xz] + node[Model::T_3][Model::xz]) / 4;
            outNode.push_back(sXX);
            outNode.push_back(sZZ);
            outNode.push_back(sXZ);
            outSub.push_back(outNode);
        }
        outStress.push_back(outSub);
    }
    return outStress;
}

//std::vector<std::pair<double, double> > ModelWriter::boundaryLine(std::shared_ptr<Model> model, ModelWriter::boundIdx bound)
//{
//    std::vector<std::pair<double, double> > outLine;
//    if (bound == ModelWriter::srf)
//    {
//        for (int i=0; i<(int)model->_cellLayerIndexes.size(); ++i)
//        {
//            for (int j=0; j<(int)model->_cellLayerIndexes[i].size(); ++j)
//            {
//                if(model->_layers.at(model->_cellLayerIndexes[i][j]) != "Air" && (j==0 || model->_layers.at(model->_cellLayerIndexes[i][j-1]) == "Air"))
//                {
//                    outLine.push_back(model->_mesh[i][j]);
//                }
//            }
//        }
//    }
//    else if (bound == ModelWriter::lab)
//    {
//        for (int i=0; i<(int)model->_cellLayerIndexes.size(); ++i)
//        {
//            for (int j=0; j<(int)model->_cellLayerIndexes[i].size(); ++j)
//            {
//                if(model->_layers.at(model->_cellLayerIndexes[i][j]) == "Astenosphere" && (j==0 || model->_layers.at(model->_cellLayerIndexes[i][j-1]) == "Lithosphere"))
//                {
//                    outLine.push_back(model->_mesh[i][j]);
//                }
//            }
//        }
//    }
//    return outLine;
//}

std::vector<std::pair<double, double> > ModelWriter::boundaryLineMovement(std::shared_ptr<Model> model, ModelWriter::boundIdx bound)
{
    std::vector<std::pair<double, double> > outLine;
    if (bound == ModelWriter::srf)
    {
        for (int i=0; i<(int)model->_cellLayerIndexes.size(); ++i)
        {
            for (int j=0; j<(int)model->_cellLayerIndexes[i].size(); ++j)
            {
                if(model->_layers.at(model->_cellLayerIndexes[i][j]) != "Air" && (j==0 || model->_layers.at(model->_cellLayerIndexes[i][j-1]) == "Air"))
                {
                    outLine.push_back(model->_mesh[i][j]);
                }
            }
        }
        if (model->srfMovement.empty())
        {
            model->srfInitial = outLine;

            for (int i=0; i<(int)outLine.size(); ++i)
            {
                double x = outLine[i].first;
                // Отрицательное смещение -- вверх, положительное -- вниз
                double z = 0.0;
                model->srfMovement.push_back(std::pair<double, double> (x,z));
            }
        }
        else
        {
            for (int i=0; i<(int)model->srfMovement.size(); ++i)
            {
                model->srfMovement[i].first = outLine[i].first;
                // Отрицательное смещение -- вверх, положительное -- вниз
                model->srfMovement[i].second = outLine[i].second - model->srfInitial[i].second;
            }
        }
        return model->srfMovement;
    }
    else if (bound == ModelWriter::lab)
    {
        for (int i=0; i<(int)model->_cellLayerIndexes.size(); ++i)
        {
            for (int j=0; j<(int)model->_cellLayerIndexes[i].size(); ++j)
            {
                if(model->_layers.at(model->_cellLayerIndexes[i][j]) == "Astenosphere" && (j==0 || model->_layers.at(model->_cellLayerIndexes[i][j-1]) == "Lithosphere"))
                {
                    outLine.push_back(model->_mesh[i][j]);
                }
            }
        }
        if (model->labMovement.empty())
        {
            model->labInitial = outLine;

            for (int i=0; i<(int)outLine.size(); ++i)
            {
                double x = outLine[i].first;
                // Отрицательное смещение -- вверх, положительное -- вниз
                double z = 0.0;
                model->labMovement.push_back(std::pair<double, double> (x,z));
            }
        }
        else
        {
            for (int i=0; i<(int)model->labMovement.size(); ++i)
            {
                model->labMovement[i].first = outLine[i].first;
                // Отрицательное смещение -- вверх, положительное -- вниз
                model->labMovement[i].second = outLine[i].second - model->labInitial[i].second;
            }
        }
        return model->labMovement;
    }
    else
        return outLine;
}

std::vector<std::pair<double, double> > ModelWriter::getLithMidLine(std::shared_ptr<Model> mdl)
{
    std::vector<std::pair<double, double> > midLine;
    for (int i=1; i<(int)mdl->_mesh.size()-1; ++i)
    {
        double xTop = .0;
        double zTop = .0;
        double xBottom = .0;
        double zBottom = .0;
#pragma omp parallel for
        for (int j=1; j<(int)mdl->_mesh[i].size()-1; ++j)
        {
            if (mdl->_layers.at(mdl->_cellLayerIndexes[i][j-1]) != "Lithosphere"
                    && mdl->_layers.at(mdl->_cellLayerIndexes[i][j]) == "Lithosphere")
            {
                xTop = mdl->_mesh[i][j].first;
                zTop = mdl->_mesh[i][j].second;
            }
            if (mdl->_layers.at(mdl->_cellLayerIndexes[i][j-1]) == "Lithosphere"
                    && mdl->_layers.at(mdl->_cellLayerIndexes[i][j]) == "Astenosphere")
            {
                xBottom = mdl->_mesh[i][j].first;
                zBottom = mdl->_mesh[i][j].second;
            }
        }
        midLine.push_back(std::pair<double, double> (0.5*(xTop+xBottom), 0.5*(zTop+zBottom)));
    }
    return midLine;
}
