#include "calculator_mechanics.h"

CalculatorMechanics::CalculatorMechanics(std::shared_ptr<Model> _mdl):
    mdl(_mdl)
{

}

void CalculatorMechanics::resetCell_dxMax()
{
    int xsize = (int)mdl->_mesh.size();
    // Заполнение dxMax для ячеек сетки
    for (int i=0; i<xsize-1; ++i)
    {
        int zsize = (int)mdl->_mesh[i].size();
        for (int j=0; j<zsize-1;++j)
        {
            double diag1 = fabs(sqrt(pow(mdl->_mesh[i+1][j+1].first - mdl->_mesh[i][j].first, 2) + pow(mdl->_mesh[i+1][j+1].second - mdl->_mesh[i][j].second, 2)));
            double diag2 = fabs(sqrt(pow(mdl->_mesh[i+1][j].first - mdl->_mesh[i][j+1].first, 2) + pow(mdl->_mesh[i+1][j].second - mdl->_mesh[i][j+1].second, 2)));
            double dxmax = std::max(diag1, diag2);
            mdl->_dxCellMax[i][j] = dxmax;
        }
    }
}

double CalculatorMechanics::maxDisplacement()
{
    double maxDispl = 0.0;
    int ii = 0;
    int jj = 0;
    for (int i=0; i<(int)mdl->_displacement.size(); ++i)
    {
        for (int j=0; j<(int)mdl->_displacement[i].size(); ++j)
        {
            double displ_i = sqrt(pow(mdl->_displacement[i][j].first, 2) + pow(mdl->_displacement[i][j].second, 2));
            if (displ_i > maxDispl)
            {
                maxDispl = displ_i;
                ii = i;
                jj = j;
            }
            //            if (maxDispl == 0.0)
            //                std::cout << "WTF??";
        }
    }
    std::cout << "\npos: " << ii << " " << jj;
    return maxDispl;
}

bool CalculatorMechanics::setStressAsLithostatic()
{
    int xmax = (int)mdl->_cells.size()-1;

    for (int i=0; i<=xmax; ++i)
    {
        int zmax = (int)mdl->_cells[i].size()-1;

#pragma omp parallel for
        for (int j=0; j<=zmax; ++j)
        {
            for (int t=0; t<(int)mdl->_stress[i][j].size(); ++t)
            {
                double nu = mdl->_nu[i][j];
                double k = nu / (1 - nu);
                // Горизонтальные литостатические напряжения
                mdl->_stress[i][j][t][Model::xx] = k * mdl->_P[i][j];
                // Вертикальные литостатические напряжения
                mdl->_stress[i][j][t][Model::zz] = mdl->_P[i][j];
                // Сдвиговые напряжения нулевые
                mdl->_stress[i][j][t][Model::xz] = 0.0;
            }
        }
    }
    return true;
}

bool CalculatorMechanics::calculate(double totalMovement, int frequency,
                                    std::pair<double, double> leftVelBC,
                                    std::pair<double, double> rightVelBC)
{
    // frequency -- частота введения ГУ (раз во сколько итераций)

    int xmax = (int)mdl->_mesh.size()-1;

    int ic = 0;
    bool stationary = false;
    //    bool maxDisplInitSet = false;
    double currentMovement = 0.0;

    mdl->labMovement.clear();
    mdl->srfMovement.clear();
    maxDispHist.clear();


    // Критерий выхода из цикла -- достижение стационара
    //    for (int iteration = 0; iteration < ic; ++iteration)
    while (!stationary)
    {
        resetCell_dxMax();
        //        maxForce = .0;
        //        maxDisp = .0;
        //        maxE = .0;
        //        maxStr = .0;
        //        minMass = 1e20;
        //        std::cout << "\r" << iteration;
        //        if (iteration == 1)
        //        {
        //            std::cout << "i = 25";
        //        }
        // Встроенная возможность останова
        if (interrupted)
        {
            interrupted = false;
            std::cout << "\nMechanical calculation has been interrupted.\n";
            break;
        }

        // Чистим вектор масс, чтобы они не разбегались
        for (int i=0; i<(int)mdl->_mass_v.size(); ++i)
        {
#pragma omp parallel for
            for (int j=0; j<(int)mdl->_mass_v[i].size(); ++j)
            {
                mdl->_mass_v[i][j] = 0.0;
            }
        }

        // Чистим вектор сил, чтобы они не разбегались
        for (int i=0; i<(int)mdl->_force.size(); ++i)
        {
#pragma omp parallel for
            for (int j=0; j<(int)mdl->_force[i].size(); ++j)
            {
                mdl->_force[i][j].first = 0.0;
                mdl->_force[i][j].second = 0.0;
                mdl->_gravity[i][j].first = 0.0;
                mdl->_gravity[i][j].second = 0.0;
            }
        }

        // Пересчитываем архимедовы силы на LAB с учётом её вертикальных смещений
        for (int i=0; i<(int)mdl->_force_archimed.size(); ++i)
        {
#pragma omp parallel for
            for (int j=0; j<(int)mdl->_force_archimed[i].size(); ++j)
            {
                // Силы архимеда заданы только для узлов в подошве литосферы.
                // Для других узлов - забиты нулями
                if (!Formulas::isNull(mdl->_force_archimed[i][j].first, 1e-7)
                        ||
                        !Formulas::isNull(mdl->_force_archimed[i][j].second, 1e-7))
                {
                    // Учёт изменения силы архимеда из-за погружени/поднятия литосферы
                    double dens = .0;
                    double a = 0.0; // Площадь ячейки

                    if (i>0 && i<(int)mdl->_force_archimed.size()-1)
                    {
                        if (j<(int)mdl->_dens[i].size()) dens = mdl->_dens[i-1][j];
                        else dens = 0.5 * mdl->_dens[i-1][j-1] * mdl->_dens[i][j-1];
                        a = mdl->_displacement[i][j].second * 0.5 * (mdl->_mesh[i+1][j].first - mdl->_mesh[i-1][j].first);
                    }
                    else if (i==0)
                    {
                        if (j<(int)mdl->_dens[i].size()) dens = mdl->_dens[0][j];
                        else dens = mdl->_dens[0][j-1];
                        a = mdl->_displacement[0][j].second * (mdl->_mesh[1][j].first - mdl->_mesh[0][j].first);
                    }
                    else if (i==(int)mdl->_force_archimed.size()-1)
                    {
                        if (j<(int)mdl->_dens[i].size()) dens = mdl->_dens[i-1][j];
                        else dens = mdl->_dens[i-1][j-1];
                        a = mdl->_displacement[i][j].second * (mdl->_mesh[i][j].first - mdl->_mesh[i-1][j].first);
                    }
//                    std::cout << "\ndens " << dens;
                    mdl->_force_archimed[i][j].second -= a * mdl->g * dens;
                }
            }
        }

        //
        for (int i=0; i<xmax; ++i)
        {
            int zmax = (int)mdl->_mesh[i].size()-1;

//#pragma omp parallel for
            for (int j=0; j<zmax; ++j)
            {
                // Проверка на нулевой материал -- это воздух или астеносфера: для них механика не считается
                if (!mdl->_layerInCalculation.at(mdl->_cellLayerIndexes[i][j])) continue;


                // Направление оси Z - вниз. j возрастает вниз
                // Треугольник T0 (верхний левый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //      0 (i, j) .____. 1 (i+1, j)
                //               |   /
                //               |  /
                //      T0       | /
                //               |/
                //               .
                //               2 (i, j+1)
*/

                double x0_T0 = mdl->_mesh[i][j].first;
                double z0_T0 = mdl->_mesh[i][j].second;
                double x1_T0 = mdl->_mesh[i+1][j].first;
                double z1_T0 = mdl->_mesh[i+1][j].second;
                double x2_T0 = mdl->_mesh[i][j+1].first;
                double z2_T0 = mdl->_mesh[i][j+1].second;
                double u0_x_T0 = mdl->_velocity[i][j].first;
                double u0_z_T0 = mdl->_velocity[i][j].second;
                double u1_x_T0 = mdl->_velocity[i+1][j].first;
                double u1_z_T0 = mdl->_velocity[i+1][j].second;
                double u2_x_T0 = mdl->_velocity[i][j+1].first;
                double u2_z_T0 = mdl->_velocity[i][j+1].second;

                double A_T0 = Formulas::A_triangle(x0_T0, z0_T0, x1_T0, z1_T0, x2_T0, z2_T0);
                double duxdz_mean_T0 = Formulas::dudz_mean_for_tringle(A_T0, x0_T0, x1_T0, x2_T0, u0_x_T0, u1_x_T0, u2_x_T0);
                double duzdx_mean_T0 = Formulas::dudx_mean_for_tringle(A_T0, z0_T0, z1_T0, z2_T0, u0_z_T0, u1_z_T0, u2_z_T0);
                double eXZ_T0 = 0.5 * (duzdx_mean_T0 + duxdz_mean_T0);
                double eXX_T0 = Formulas::dudx_mean_for_tringle(A_T0, z0_T0, z1_T0, z2_T0, u0_x_T0, u1_x_T0, u2_x_T0);
                double eZZ_T0 = Formulas::dudz_mean_for_tringle(A_T0, x0_T0, x1_T0, x2_T0, u0_z_T0, u1_z_T0, u2_z_T0);

                // -------------------------------------------------------------------------------------------------------------------------------------

                // Треугольник T1 (нижний правый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //         0 (i+1, j) .
                //                   /|
                //                  / |
                //         T1      /  |
                //                /   |
                //               .____.
                //     2 (i, j+1)       1 (i+1, j+1)
*/
                double x0_T1 = mdl->_mesh[i+1][j].first;
                double z0_T1 = mdl->_mesh[i+1][j].second;
                double x1_T1 = mdl->_mesh[i+1][j+1].first;
                double z1_T1 = mdl->_mesh[i+1][j+1].second;
                double x2_T1 = mdl->_mesh[i][j+1].first;
                double z2_T1 = mdl->_mesh[i][j+1].second;
                double u0_x_T1 = mdl->_velocity[i+1][j].first;
                double u0_z_T1 = mdl->_velocity[i+1][j].second;
                double u1_x_T1 = mdl->_velocity[i+1][j+1].first;
                double u1_z_T1 = mdl->_velocity[i+1][j+1].second;
                double u2_x_T1 = mdl->_velocity[i][j+1].first;
                double u2_z_T1 = mdl->_velocity[i][j+1].second;

                double A_T1 = Formulas::A_triangle(x0_T1, z0_T1, x1_T1, z1_T1, x2_T1, z2_T1);
                double duxdz_mean_T1 = Formulas::dudz_mean_for_tringle(A_T1, x0_T1, x1_T1, x2_T1, u0_x_T1, u1_x_T1, u2_x_T1);
                double duzdx_mean_T1 = Formulas::dudx_mean_for_tringle(A_T1, z0_T1, z1_T1, z2_T1, u0_z_T1, u1_z_T1, u2_z_T1);
                double eXZ_T1 = 0.5 * (duzdx_mean_T1 + duxdz_mean_T1);
                double eXX_T1 = Formulas::dudx_mean_for_tringle(A_T1, z0_T1, z1_T1, z2_T1, u0_x_T1, u1_x_T1, u2_x_T1);
                double eZZ_T1 = Formulas::dudz_mean_for_tringle(A_T1, x0_T1, x1_T1, x2_T1, u0_z_T1, u1_z_T1, u2_z_T1);

                // -------------------------------------------------------------------------------------------------------------------------------------

                // Треугольник T2 (верхний правый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //      0 (i, j) .____. 1 (i+1, j)
                //                \   |
                //                 \  |
                //       T2         \ |
                //                   \|
                //                    .
                //                    2 (i+1, j+1)
*/
                double x0_T2 = mdl->_mesh[i][j].first;
                double z0_T2 = mdl->_mesh[i][j].second;
                double x1_T2 = mdl->_mesh[i+1][j].first;
                double z1_T2 = mdl->_mesh[i+1][j].second;
                double x2_T2 = mdl->_mesh[i+1][j+1].first;
                double z2_T2 = mdl->_mesh[i+1][j+1].second;
                double u0_x_T2 = mdl->_velocity[i][j].first;
                double u0_z_T2 = mdl->_velocity[i][j].second;
                double u1_x_T2 = mdl->_velocity[i+1][j].first;
                double u1_z_T2 = mdl->_velocity[i+1][j].second;
                double u2_x_T2 = mdl->_velocity[i+1][j+1].first;
                double u2_z_T2 = mdl->_velocity[i+1][j+1].second;

                double A_T2 = Formulas::A_triangle(x0_T2, z0_T2, x1_T2, z1_T2, x2_T2, z2_T2);
                double duxdz_mean_T2 = Formulas::dudz_mean_for_tringle(A_T2, x0_T2, x1_T2, x2_T2, u0_x_T2, u1_x_T2, u2_x_T2);
                double duzdx_mean_T2 = Formulas::dudx_mean_for_tringle(A_T2, z0_T2, z1_T2, z2_T2, u0_z_T2, u1_z_T2, u2_z_T2);
                double eXZ_T2 = 0.5 * (duzdx_mean_T2 + duxdz_mean_T2);
                double eXX_T2 = Formulas::dudx_mean_for_tringle(A_T2, z0_T2, z1_T2, z2_T2, u0_x_T2, u1_x_T2, u2_x_T2);
                double eZZ_T2 = Formulas::dudz_mean_for_tringle(A_T2, x0_T2, x1_T2, x2_T2, u0_z_T2, u1_z_T2, u2_z_T2);

                // -------------------------------------------------------------------------------------------------------------------------------------

                // Треугольник T3 (нижний левый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //      0 (i, j) .
                //               |\
                //               | \
                //       T3      |  \
                //               |   \
                //               .----.
                //     2 (i, j+1)      1 (i+1, j+1)
*/
                double x0_T3 = mdl->_mesh[i][j].first;
                double z0_T3 = mdl->_mesh[i][j].second;
                double x1_T3 = mdl->_mesh[i+1][j+1].first;
                double z1_T3 = mdl->_mesh[i+1][j+1].second;
                double x2_T3 = mdl->_mesh[i][j+1].first;
                double z2_T3 = mdl->_mesh[i][j+1].second;
                double u0_x_T3 = mdl->_velocity[i][j].first;
                double u0_z_T3 = mdl->_velocity[i][j].second;
                double u1_x_T3 = mdl->_velocity[i+1][j+1].first;
                double u1_z_T3 = mdl->_velocity[i+1][j+1].second;
                double u2_x_T3 = mdl->_velocity[i][j+1].first;
                double u2_z_T3 = mdl->_velocity[i][j+1].second;

                double A_T3 = Formulas::A_triangle(x0_T3, z0_T3, x1_T3, z1_T3, x2_T3, z2_T3);
                double duxdz_mean_T3 = Formulas::dudz_mean_for_tringle(A_T3, x0_T3, x1_T3, x2_T3, u0_x_T3, u1_x_T3, u2_x_T3);
                double duzdx_mean_T3 = Formulas::dudx_mean_for_tringle(A_T3, z0_T3, z1_T3, z2_T3, u0_z_T3, u1_z_T3, u2_z_T3);
                double eXZ_T3 = 0.5 * (duzdx_mean_T3 + duxdz_mean_T3);
                double eXX_T3 = Formulas::dudx_mean_for_tringle(A_T3, z0_T3, z1_T3, z2_T3, u0_x_T3, u1_x_T3, u2_x_T3);
                double eZZ_T3 = Formulas::dudz_mean_for_tringle(A_T3, x0_T3, x1_T3, x2_T3, u0_z_T3, u1_z_T3, u2_z_T3);

                // Усредняем по 4 треугольникам средние компоненты тензоров скоростей деформаций
                // Это нужно, чтобы не возникало численной анизотропии

                double e_mean_T0 = 0.5 * (eXX_T0 + eZZ_T0);
                double e_mean_T1 = 0.5 * (eXX_T1 + eZZ_T1);
                double e_mean_T2 = 0.5 * (eXX_T2 + eZZ_T2);
                double e_mean_T3 = 0.5 * (eXX_T3 + eZZ_T3);
                double e_mean_mean = 0.25 * (e_mean_T0 + e_mean_T1 + e_mean_T2 + e_mean_T3);

                eXX_T0 = eXX_T0 - e_mean_T0 + e_mean_mean;
                eZZ_T0 = eZZ_T0 - e_mean_T0 + e_mean_mean;
                eXZ_T0 = eXZ_T0 - e_mean_T0 + e_mean_mean;

                eXX_T1 = eXX_T1 - e_mean_T1 + e_mean_mean;
                eZZ_T1 = eZZ_T1 - e_mean_T1 + e_mean_mean;
                eXZ_T1 = eXZ_T1 - e_mean_T1 + e_mean_mean;

                eXX_T2 = eXX_T2 - e_mean_T2 + e_mean_mean;
                eZZ_T2 = eZZ_T2 - e_mean_T2 + e_mean_mean;
                eXZ_T2 = eXZ_T2 - e_mean_T2 + e_mean_mean;

                eXX_T3 = eXX_T3 - e_mean_T3 + e_mean_mean;
                eZZ_T3 = eZZ_T3 - e_mean_T3 + e_mean_mean;
                eXZ_T3 = eXZ_T3 - e_mean_T3 + e_mean_mean;

                //                maxE = std::max(maxE, sqrt(eXX_T0*eXX_T0 + eZZ_T0*eZZ_T0 + eXZ_T0*eXZ_T0));
                //                maxE = std::max(maxE, sqrt(eXX_T0*eXX_T1 + eZZ_T0*eZZ_T1 + eXZ_T0*eXZ_T1));
                //                maxE = std::max(maxE, sqrt(eXX_T0*eXX_T2 + eZZ_T0*eZZ_T2 + eXZ_T0*eXZ_T2));
                //                maxE = std::max(maxE, sqrt(eXX_T0*eXX_T3 + eZZ_T0*eZZ_T3 + eXZ_T0*eXZ_T3));

                // Рассчитываем тензоры напряжений и силы для каждого треугольника на основе пересчитанных скоростей деформаций
                // Направление обхода -- по часовой стрелке

                // Координаты узлов дополнительной сетки
                std::pair<double,double> cellCenter = Formulas::cell_ij_center_pos(mdl->_mesh, i, j);
                std::pair<double,double> edgeCenter_left = Formulas::edge_center_pos(mdl->_mesh, i, j, i, j+1);
                std::pair<double,double> edgeCenter_right = Formulas::edge_center_pos(mdl->_mesh, i+1, j, i+1, j+1);
                std::pair<double,double> edgeCenter_top = Formulas::edge_center_pos(mdl->_mesh, i, j, i+1, j);
                std::pair<double,double> edgeCenter_bottom = Formulas::edge_center_pos(mdl->_mesh, i, j+1, i+1, j+1);

                // Треугольник T0 (верхний левый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //      0 (i, j) .____. 1 (i+1, j)
                //               |   /
                //               |  /
                //       T0      | /
                //               |/
                //               .
                //               2 (i, j+1)
*/
                double stressXX_T0 = mdl->_stress[i][j][Model::T_0][Model::xx] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_0][Model::xx], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T0, eZZ_T0, eXX_T0);
                double stressZZ_T0 = mdl->_stress[i][j][Model::T_0][Model::zz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_0][Model::zz], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T0, eZZ_T0, eZZ_T0);
                double stressXZ_T0 = mdl->_stress[i][j][Model::T_0][Model::xz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_0][Model::xz], mdl->_E[i][j], mdl->_nu[i][j], 0, eXX_T0, eZZ_T0, eXZ_T0);

                std::pair<double,double> force_T0_eTC = Formulas::F_edge(edgeCenter_top.first, edgeCenter_top.second, cellCenter.first, cellCenter.second,
                                                                         stressXX_T0, stressZZ_T0, stressXZ_T0);
                std::pair<double,double> force_T0_eCL = Formulas::F_edge(cellCenter.first, cellCenter.second,  edgeCenter_left.first, edgeCenter_left.second,
                                                                         stressXX_T0, stressZZ_T0, stressXZ_T0);

                // Треугольник T1 (нижний правый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //         0 (i+1, j) .
                //                   /|
                //                  / |
                //         T1      /  |
                //                /   |
                //               .____.
                //     2 (i, j+1)       1 (i+1, j+1)
*/
                double stressXX_T1 = mdl->_stress[i][j][Model::T_1][Model::xx] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_1][Model::xx], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T1, eZZ_T1, eXX_T1);
                double stressZZ_T1 = mdl->_stress[i][j][Model::T_1][Model::zz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_1][Model::zz], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T1, eZZ_T1, eZZ_T1);
                double stressXZ_T1 = mdl->_stress[i][j][Model::T_1][Model::xz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_1][Model::xz], mdl->_E[i][j], mdl->_nu[i][j], 0, eXX_T1, eZZ_T1, eXZ_T1);

                std::pair<double,double> force_T1_eBC = Formulas::F_edge(edgeCenter_bottom.first, edgeCenter_bottom.second, cellCenter.first, cellCenter.second,
                                                                         stressXX_T1, stressZZ_T1, stressXZ_T1);
                std::pair<double,double> force_T1_eCR = Formulas::F_edge(cellCenter.first, cellCenter.second, edgeCenter_right.first, edgeCenter_right.second,
                                                                         stressXX_T1, stressZZ_T1, stressXZ_T1);

                // Треугольник T2 (верхний правый)
                /*
                // Направление обхода -- по часовой стреке
                //
                //      0 (i, j) .____. 1 (i+1, j)
                //                \   |
                //                 \  |
                //       T2         \ |
                //                   \|
                //                    .
                //                    2 (i+1, j+1)
*/
                double stressXX_T2 = mdl->_stress[i][j][Model::T_2][Model::xx] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_2][Model::zz], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T2, eZZ_T2, eXX_T2);
                double stressZZ_T2 = mdl->_stress[i][j][Model::T_2][Model::zz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_2][Model::zz], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T2, eZZ_T2, eZZ_T2);
                double stressXZ_T2 = mdl->_stress[i][j][Model::T_2][Model::xz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_2][Model::xz], mdl->_E[i][j], mdl->_nu[i][j], 0, eXX_T2, eZZ_T2, eXZ_T2);

                std::pair<double,double> force_T2_eRC = Formulas::F_edge(edgeCenter_right.first, edgeCenter_right.second, cellCenter.first, cellCenter.second,
                                                                         stressXX_T2, stressZZ_T2, stressXZ_T2);
                std::pair<double,double> force_T2_eCT = Formulas::F_edge(cellCenter.first, cellCenter.second, edgeCenter_top.first, edgeCenter_top.second,
                                                                         stressXX_T2, stressZZ_T2, stressXZ_T2);

                // Треугольник T3 (нижний левый)
                //
                // Направление обхода -- по часовой стреке
                /*
                //      0 (i, j) .
                //               |\
                //               | \
                //       T3      |  \
                //               |   \
                //               .----.
                //     2 (i, j+1)      1 (i+1, j+1)
*/
                double stressXX_T3 = mdl->_stress[i][j][Model::T_3][Model::xx] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_3][Model::xx], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T3, eZZ_T3, eXX_T3);
                double stressZZ_T3 = mdl->_stress[i][j][Model::T_3][Model::zz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_3][Model::zz], mdl->_E[i][j], mdl->_nu[i][j], 1, eXX_T3, eZZ_T3, eZZ_T3);
                double stressXZ_T3 = mdl->_stress[i][j][Model::T_3][Model::xz] = Formulas::stress_ij_next(mdl->_stress[i][j][Model::T_3][Model::xz], mdl->_E[i][j], mdl->_nu[i][j], 0, eXX_T3, eZZ_T3, eXZ_T3);

                std::pair<double,double> force_T3_eLC = Formulas::F_edge(edgeCenter_left.first, edgeCenter_left.second, cellCenter.first, cellCenter.second,
                                                                         stressXX_T3, stressZZ_T3, stressXZ_T3);
                std::pair<double,double> force_T3_eCB = Formulas::F_edge(cellCenter.first, cellCenter.second, edgeCenter_bottom.first, edgeCenter_bottom.second,
                                                                         stressXX_T3, stressZZ_T3, stressXZ_T3);


                //                maxStr = std::max(maxStr, sqrt(stressXX_T0*stressXX_T0 + stressZZ_T0*stressZZ_T0 + stressXZ_T0*stressXZ_T0));
                //                maxStr = std::max(maxStr, sqrt(stressXX_T0*stressXX_T1 + stressZZ_T0*stressZZ_T1 + stressXZ_T0*stressXZ_T1));
                //                maxStr = std::max(maxStr, sqrt(stressXX_T0*stressXX_T2 + stressZZ_T0*stressZZ_T2 + stressXZ_T0*stressXZ_T2));
                //                maxStr = std::max(maxStr, sqrt(stressXX_T0*stressXX_T3 + stressZZ_T0*stressZZ_T3 + stressXZ_T0*stressXZ_T3));

                // Раскидываем силы в узлы из каждого треугольника

                // Треугольник T0 (верхний левый)

                mdl->_force[i+1][j].first -= force_T0_eTC.first;
                mdl->_force[i+1][j].second -= force_T0_eTC.second;

                mdl->_force[i][j].first += force_T0_eTC.first;
                mdl->_force[i][j].second += force_T0_eTC.second;
                mdl->_force[i][j].first += force_T0_eCL.first;
                mdl->_force[i][j].second += force_T0_eCL.second;

                mdl->_force[i][j+1].first -= force_T0_eCL.first;
                mdl->_force[i][j+1].second -= force_T0_eCL.second;

                // Треугольник T1 (нижний правый)

                mdl->_force[i][j+1].first -= force_T1_eBC.first;
                mdl->_force[i][j+1].second -= force_T1_eBC.second;

                mdl->_force[i+1][j+1].first += force_T1_eBC.first;
                mdl->_force[i+1][j+1].second += force_T1_eBC.second;
                mdl->_force[i+1][j+1].first += force_T1_eCR.first;
                mdl->_force[i+1][j+1].second += force_T1_eCR.second;

                mdl->_force[i+1][j].first -= force_T1_eCR.first;
                mdl->_force[i+1][j].second -= force_T1_eCR.second;


                // Треугольник T2 (верхний правый)

                mdl->_force[i+1][j+1].first -= force_T2_eRC.first;
                mdl->_force[i+1][j+1].second -= force_T2_eRC.second;

                mdl->_force[i+1][j].first += force_T2_eRC.first;
                mdl->_force[i+1][j].second += force_T2_eRC.second;
                mdl->_force[i+1][j].first += force_T2_eCT.first;
                mdl->_force[i+1][j].second += force_T2_eCT.second;

                mdl->_force[i][j].first -= force_T2_eCT.first;
                mdl->_force[i][j].second -= force_T2_eCT.second;

                // Треугольник T3 (нижний левый)
                mdl->_force[i][j].first -= force_T3_eLC.first;
                mdl->_force[i][j].second -= force_T3_eLC.second;

                mdl->_force[i][j+1].first += force_T3_eLC.first;
                mdl->_force[i][j+1].second += force_T3_eLC.second;
                mdl->_force[i][j+1].first += force_T3_eCB.first;
                mdl->_force[i][j+1].second += force_T3_eCB.second;

                mdl->_force[i+1][j+1].first -= force_T3_eCB.first;
                mdl->_force[i+1][j+1].second -= force_T3_eCB.second;



                if (std::isnan(mdl->_force[i][j].first) || std::isnan(mdl->_force[i][j].second) ||
                        std::isnan(mdl->_force[i][j+1].first) || std::isnan(mdl->_force[i][j+1].second) ||
                        std::isnan(mdl->_force[i+1][j].first) || std::isnan(mdl->_force[i+1][j].second) ||
                        std::isnan(mdl->_force[i+1][j+1].first) || std::isnan(mdl->_force[i+1][j+1].second))
                    //                    if (i==0 && j==0)
                    std::cout << "shit";



                // Раскидываем массы в узлы
                mdl->_mass_v[i][j] += (Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T0)
                                       +
                                       Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T1)
                                       +
                                       Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T3)) / 3;

                mdl->_mass_v[i+1][j] += (Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T0)
                                         +
                                         Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T1)
                                         +
                                         Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T2)) / 3;

                mdl->_mass_v[i][j+1] += (Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T0)
                                         +
                                         Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T1)
                                         +
                                         Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T3)) / 3;

                mdl->_mass_v[i+1][j+1] +=  (Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T1)
                                            +
                                            Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T2)
                                            +
                                            Formulas::mass_triangle(mdl->_K[i][j], mdl->_G[i][j], mdl->_dxCellMax[i][j], A_T3)) / 3;

            }
        }

        // Приводим в порядок силы
        for (int i=0; i<=xmax; ++i)
        {
            int zmax = (int)mdl->_mesh[i].size()-1;
#pragma omp parallel for
            for (int j=0; j<=zmax; ++j)
            {
                // Сила в узле вдоль соответствующей координаты делится на два, поскольку значение посчитано дважды:
                // по разу из каждой пары треугольников
                mdl->_force[i][j].first /= 2;
                mdl->_force[i][j].second /= 2;

//                // Учитываем силу Архимеда
                mdl->_force[i][j].first += mdl->_force_archimed[i][j].first;
                mdl->_force[i][j].second += mdl->_force_archimed[i][j].second;
            }
        }

        // Приводим в порядок массы
#pragma omp parallel for
        for (int j=0; j<=(int)mdl->_mass_v[0].size(); ++j)
        {
            mdl->_mass_v[0][j] *= 2;
            mdl->_mass_v[xmax][j] *= 2;
        }

//        // Добавляемм гравитацию
        for (int i=0; i<(int)mdl->_cells.size(); ++i)
        {
#pragma omp parallel for
            for (int j=0; j<(int)mdl->_cells[i].size(); ++j)
            {
                // Координаты узлов дополнительной сетки
                std::pair<double,double> cellCenter = Formulas::cell_ij_center_pos(mdl->_mesh, i, j);
                std::pair<double,double> edgeCenter_left = Formulas::edge_center_pos(mdl->_mesh, i, j+1, i, j);
                std::pair<double,double> edgeCenter_right = Formulas::edge_center_pos(mdl->_mesh, i+1, j+1, i+1, j);
                std::pair<double,double> edgeCenter_top = Formulas::edge_center_pos(mdl->_mesh, i+1, j, i, j);
                std::pair<double,double> edgeCenter_bottom = Formulas::edge_center_pos(mdl->_mesh, i+1, j+1, i, j+1);
                double A_tl = Formulas::A_tetragon(mdl->_mesh[i][j], edgeCenter_top, edgeCenter_left, cellCenter);
                double A_tr = Formulas::A_tetragon(edgeCenter_top, mdl->_mesh[i+1][j], cellCenter, edgeCenter_right);
                double A_bl = Formulas::A_tetragon(edgeCenter_left, cellCenter, mdl->_mesh[i][j+1], edgeCenter_bottom);
                double A_br = Formulas::A_tetragon(cellCenter, edgeCenter_right, edgeCenter_bottom, mdl->_mesh[i+1][j+1]);

                mdl->_gravity[i][j].second += mdl->g * mdl->_dens[i][j] * A_tl;
                mdl->_gravity[i+1][j].second += mdl->g * mdl->_dens[i][j] * A_tr;
                mdl->_gravity[i][j+1].second += mdl->g * mdl->_dens[i][j] * A_bl;
                mdl->_gravity[i+1][j+1].second += mdl->g * mdl->_dens[i][j] * A_br;
                // Поскольку на краевые узлы модели гравитация учитывается не полностью,
                // удвоим силу тяжести для узлов на границах
                if (i==0)
                {
                    mdl->_gravity[i][j].second += mdl->g * mdl->_dens[i][j] * A_tl;
                    mdl->_gravity[i][j+1].second += mdl->g * mdl->_dens[i][j] * A_bl;
                }
                if (i==(int)mdl->_cells.size()-1)
                {
                    mdl->_gravity[i+1][j].second += mdl->g * mdl->_dens[i][j] * A_tr;
                    mdl->_gravity[i+1][j+1].second += mdl->g * mdl->_dens[i][j] *A_br ;
                }
            }
        }

//        for (int i=0; i<=xmax; ++i)
//        {
//            int zmax = mdl->_mesh[i].size()-1;
//            double gravSum = 0.0;
//#pragma omp parallel for
//            for (int j=0; j<=zmax; ++j)
//            {
//                gravSum += mdl->_gravity[i][j].second;
//                if (j < zmax
//                        && mdl->_layers.at(mdl->_cellLayerIndexes[i][j-1]) == "Lithosphere"
//                        && mdl->_layers.at(mdl->_cellLayerIndexes[i][j]) == "Astenosphere")
//                {
//                    std::cout << "\nArchimed Z: " <<  mdl->_force_archimed[i][j].second;
//                    std::cout << "\nSum gravity Z: " << gravSum << "\n";
//                }
//            }
//        }

        // Рассчёт ускорений, скоростей и смещений в узлах
        for (int i=0; i<=xmax; ++i)
        {
            int zmax = (int)mdl->_mesh[i].size()-1;

#pragma omp parallel for
            for (int j=0; j<=zmax; ++j)
            {
                // Проверка, чтобы считать только узлы ячеек, участвующих в расчёте
                if (i==0)
                {
                    if (j==0)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j]])
                            continue;
                    }
                    else if (j==zmax)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j-1]])
                            continue;
                    }
                    else
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j-1]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j]])
                            continue;
                    }
                }
                else if (i==xmax)
                {
                    if (j==0)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j]])
                            continue;
                    }
                    else if (j==zmax)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j-1]])
                            continue;
                    }
                    else
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j-1]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j]])
                            continue;
                    }
                }
                else
                {
                    if (j==0)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j]])
                            continue;
                    }
                    else if (j==zmax)
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j-1]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j-1]])
                            continue;
                    }
                    else
                    {
                        if (!mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j-1]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i-1][j]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j-1]]
                                &&
                                !mdl->_layerInCalculation[mdl->_cellLayerIndexes[i][j]])
                            continue;
                    }
                }
                // Вводим в баланс сил гравитацию
                mdl->_force[i][j].first += mdl->_gravity[i][j].first;
                mdl->_force[i][j].second += mdl->_gravity[i][j].second;


                // Ввод демпфинга
                mdl->_force[i][j].first += Formulas::F_damp_xi(mdl->_force[i][j].first, mdl->_velocity[i][j].first);
                mdl->_force[i][j].second += Formulas::F_damp_xi(mdl->_force[i][j].second, mdl->_velocity[i][j].second);

                // Рассчёт массы узла через треугольники (по документации Itasca FLAC 8 Theory and Background: 1.3.5)
                // Усредняем по двум разбиениям (делим на 2, если не делим, значит, уже введён safety factor=2 )
                double mass_nodal =/* 0.5 * */std::max(mdl->_mass_v[i][j], .0);

                // Пересчёт ускорений
                mdl->_gradients_of_velocity[i][j].first = Formulas::node_acceleration_xi(mdl->_force[i][j].first, mass_nodal);
                mdl->_gradients_of_velocity[i][j].second = Formulas::node_acceleration_xi(mdl->_force[i][j].second, mass_nodal);

                // Пересчёт скоростей и смещений и координат узлов

                // потому что dt = 1 c
                mdl->_velocity[i][j].first += mdl->_gradients_of_velocity[i][j].first;
                mdl->_velocity[i][j].second += mdl->_gradients_of_velocity[i][j].second;

                if (frequency > 0)
                {
                    if (ic%frequency == 0 && fabs(currentMovement) < fabs(totalMovement))
                    {
                        if (i==0)
                        {
                            mdl->_velocity[i][j].first = leftVelBC.first;
                            //                            mdl->_velocity[i][j].second = leftVelBC.second;
                        }
                        if (i==xmax)
                        {
                            mdl->_velocity[i][j].first = rightVelBC.first;
                            //                            mdl->_velocity[i][j].second = rightVelBC.second;
                        }
                    }
                    else
                    {
                        if (i==0)
                        {
                            mdl->_velocity[i][j].first = 0.0;
                            //                            mdl->_velocity[i][j].second = 0.0;
                        }
                        if (i==xmax)
                        {
                            mdl->_velocity[i][j].first = 0.0;
                            //                            mdl->_velocity[i][j].second = 0.0;
                        }
                    }
                }

                mdl->_displacement[i][j].first = mdl->_velocity[i][j].first;
                mdl->_displacement[i][j].second = mdl->_velocity[i][j].second;


                //                if (i<xmax)
                //                {
                //                    double absDisp = sqrt((mdl->_displacement[i][j].first * mdl->_displacement[i][j].first)
                //                                          +
                //                                          (mdl->_displacement[i][j].second * mdl->_displacement[i][j].second));
                //                    if (absDisp > maxDisp)
                //                        maxDisp = absDisp;
                //                }

                mdl->_mesh[i][j].first += mdl->_displacement[i][j].first;
                mdl->_mesh[i][j].second += mdl->_displacement[i][j].second;
                if(i>0 && j>0)
                {
                    mdl->_cells[i-1][j-1] = Formulas::cell_ij_center_pos(mdl->_mesh, i, j);
                }

                interrupted = std::isnan(mdl->_mesh[i][j].first) || std::isnan(mdl->_mesh[i][j].second);
                //                if (interrupted == true)
                //                {
                //                    std::cout << "WTF";
                //                }
            }
        }

        //        std::cout << "\nStep: " << iteration << "\t Max Strain Rate: " << maxE;
        //        std::cout << "\nStep: " << iteration << "\t Max Stress: " << maxStr;
        //        std::cout << "\nStep: " << iteration << "\t Max Force: " << maxForce;
        //        std::cout << "\nStep: " << iteration << "\t Max Displacement: " << maxDisp;
        //                std::cout << "\nStep: " << iteration << "\t Min Mass: " << minMass;// << ";

        // Пересчёт dxMax для ячеек сетки
        //        resetCell_dxMax();

        // Максималное смещение на текущем шаге
        double maxDispl = maxDisplacement();
        maxDispHist.push_back(std::pair<int, double> (ic, maxDispl));
        if (frequency == 0)
        {
            ParametersWriter::writePolyLine("../output/maxDispl_eq.txt", maxDispHist);
        }
        else if (frequency > 0)
        {
            ParametersWriter::writePolyLine("../output/maxDispl_mv.txt", maxDispHist);
        }

        std::cout << "\n" << ic << " Max Displacement: " << maxDispl << " m";
        if (frequency > 0 && ic%frequency == 0 && fabs(currentMovement) <= fabs(totalMovement))
        {
            currentMovement += fabs(leftVelBC.first);
            std::cout << "\nAddition the condition.";
            std::cout << "\nMovement: " << fabs(currentMovement) << "/" << fabs(totalMovement) << "\n";
        }

        // По ходу расчёта ведётся запись данных
        //        if (frequency > 0)
        //        {
        if (ic>0 && ic%100==0)
        {

            int i = ModelWriter::writeModel(mdl);
            std::cout << "\n" <<i;
            //            std::cout << "\nData written, " << UTCTimeEnd << "\n";
        }
        //        }
        ++ic;

        // Проверка на стационарность
        if (maxDispl <= mdl->stationarCriterion)
        {
            std::cout << "\nStationary has been riched!";
            stationary = true;
        }
        //        if(frequency == 0 && ic >= 2e4)
        //        {
        //            stationary = true;
        //        }
    }
    std::cout << "\nNumber of iteration has been required to reach stationarity: " << ic;
    return true;
}
