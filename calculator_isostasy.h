#ifndef CALCULATOR_ISOSTASY_H
#define CALCULATOR_ISOSTASY_H

#include <iostream>

#include "formulas.h"
#include "model.h"
#include "calculator_thermal.h"


class CalculatorIsostasy
{
public:

    CalculatorIsostasy(std::shared_ptr<Model> _mdl);

    bool runIsostate ();
    void resetP (int i);


private:

std::shared_ptr<Model> mdl;


};

#endif // CALCULATOR_ISOSTASY_H
