QT -= gui

CONFIG += c++20 console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS +=  -fopenmp
LIBS += -lgomp -lpthread

SOURCES += main.cpp \
    meshgenerator.cpp \
    modelgeometryreader.cpp \
    parameterswriter.cpp \
    model.cpp \
    modelsetupreader.cpp \
    calculator_mechanics.cpp \
    calculator_thermal.cpp \
    calculator_isostasy.cpp \
    modelwriter.cpp

HEADERS += \
    meshgenerator.h \
    modelgeometryreader.h \
    formulas.h \
    parameterswriter.h \
    model.h \
    modelsetupreader.h \
    calculator_mechanics.h \
    calculator_thermal.h \
    calculator_isostasy.h \
    modelwriter.h
