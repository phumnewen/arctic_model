#ifndef FORMULAS_H
#define FORMULAS_H

#include <memory>
#include <limits>
#include <vector>
#include <cmath>
#include <iostream>

class Formulas
{

public:

    Formulas();


    static constexpr double prec = std::numeric_limits<double>::epsilon();

    // БЛАНКОВАННЫЕ ЗНАЧЕНИЯ -- то есть невалидные, те, которыми забиваются неинициализированные переменные

    // Это константа для проверки: если число больше, чем она -- значит число BLANK
    static constexpr double MY_BLANK_MIN = 1.6e38;
    // Это константа, которая представляет собой просто очень большое некруглое число, которое заведомо невалидно
    static constexpr double MY_BLANK = 1.70141e38;


    // Сравнение double'ов с нулём
    static inline bool isNull(double num, double precision=prec)
    {
        if (fabs(num - 0) <= precision)
            return true;
        return false;
    }

    // Сравнение double'ов с заданной точностью
    static inline bool areEqual(double num1, double num2, double precision=prec)
    {
        if (isNull(num1) && isNull(num2))
            return true;
        if (isNull(num1) || isNull(num2))
            return false;
        if (fabs(num1-num2) < fabs(precision))
            return true;
        return false;
    }

    // Проверка на невалидное значение, то есть BLANK
    static inline bool isBlank(double num)
    {
        if (num > MY_BLANK_MIN)
            return true;
        return false;
    }

    // ---------------------------------------------------

    // Перевод миллионов лет в секунды
    static inline double myToSec (double my)
    {
        double sec = my*1e6*365*24*3600;
        std::cout << "\nseconds: " << sec << std::endl;
        return my*1e6*365*24*3600;
    }

    // Перевод см/год в м/с
    static inline double cmpyToMps (double cm)
    {
        double m = (cm/100) / (365*24*3600);
        std::cout << "\nm/s: " << m << std::endl;
        return cm/(100.*365.*24.*3600.);
    }

    // Коэффициент теплопроводности от температуры
    static inline double lambda (double T, double P, double addend, double numerator, bool isMantle = false)
    {
        // Множитель 1e-6 при давлении из-за того, что оно берётся в МПа
        return (addend + numerator / (T+77)) * (1 + 4e-5 * 1e-6 * P * (int)isMantle);
    }

    // Символ кронекера
    static inline int kronecker(int i, int j)
    {
        return (int) i == j;
    }

    // Молуль Юнга, Па
    static inline double E (double dens, double v_p, double v_s)
    {
        // dens - плотность, кг/м3
        // v_p, v_s - скорости продольных и поперечных волн, м/с

        if (isNull(v_p) || isNull(v_s) || isNull(dens))
        {
            return 0;
        }
        return 0.5 * dens * v_s*v_s * ( 3 * v_p*v_p - 4. * v_s*v_s) / (v_p*v_p - v_s*v_s);
    }

    // Коэффициент Пуассона
    static inline double nu (double v_p, double v_s)
    {
        // v_p, v_s - скорости продольных и поперечных волн, м/с

        if (isNull(v_p) || isNull(v_s))
        {
            return 0;
        }
        return (0.5 * v_p*v_p - v_s*v_s) / (v_p*v_p - v_s*v_s);
    }

    // Модуль сдвига, Па
    static inline double G (double v, double E)
    {
        // v - коэффициент Пуассона, безразмерный
        // E - модуль Юнга, Па

        //        double var = E / (2. * (1. + v));
        //        if (std::isnan(var))
        //        {
        //            std::cout << "";
        //        }
        //        return var;

        if (areEqual(v, 1.0))
            return MY_BLANK;
        return E / (2. * (1. + v));
    }

    // Модуль объёмной упругости, Па
    static inline double K (double v, double E)
    {
        // v - коэффициент Пуассона, безразмерный
        // E - модуль Юнга, Па
        if (areEqual(v, 0.5))
            return MY_BLANK;
        return E / (3. * (1. - 2. * v));
    }


    //Коэффициент Ламе L, Па
    static inline double lameL (double v, double E)
    {
        // v - коэффициент Пуассона, безразмерный
        // E - модуль Юнга, Па
        return E * v / ((1 + v) * (1. - 2.*v));
    }

    // Какая-то модификация коэффициентов Ламе
    static inline double lameL_other (double v, double E)
    {
        // v - коэффициент Пуассона, безразмерный
        // E - модуль Юнга, Па

        //        double lame_g = lameG(v, E);
        //        double lame_l = lameL(v, E);
        //        double lame = lame_l * (1 - lame_l / (2 * lame_g + lame_l));
        //        if (std::isnan(lame))
        //        {
        //            std::cout <<"";
        //        }
        //        return lame;
//        double ggg = G(v, E);
//        double lll=lameL(v, E) * (1. - ( lameL(v, E) / (2. * G(v, E) + lameL(v, E)) ));
        return lameL(v, E) * (1. - ( lameL(v, E) / (2. * G(v, E) + lameL(v, E)) ));
    }

    // Напряжение на следующем шаге по времени в треугольной зоне
    static inline double stress_ij_next (double stress_ij, double E, double nu, int kron_ij, double e_11, double e_22, double  e_ij)
    {
        // nu - коэффициент Пуассона, безразмерный
        // E - модуль Юнга, Па

//        if (kron_ij !=0 && kron_ij != 1)
//            return .0;

//        double lame_g = G(nu, E);
//        double lame_l = lameL_other(nu, E);
//        double stress = stress_ij + (lame_l * (e_11 + e_22) * kron_ij + 2 * lame_g * e_ij);
//        if (std::isnan(stress))
//        {
//            std::cout << "stress_ij is NULL!";
//        }
//        return stress;

        // В формуле присутствует условный шаг по времени dt как множитель перед скобкой,
        // но мы принимаем его равным 1, а для сходимости рассчитываем условные массы,
        // исходя из этого шага по времени (см. комментарий к функции node_acceleration_xi)
        return stress_ij + lameL_other(nu, E) * (e_11 + e_22) * kron_ij + 2. * G(nu, E) * e_ij;
    }

    // Пластическая деформация
//    static inline double delta_strain_plastic (std::vector<double> delta_stress_ij, double E, double nu, int kron_ij)
//    {
//        return ;
//    }

    // Площадь треугольника
    static inline double A_triangle (double x0, double y0, double x1, double y1, double x2, double y2)
    {
        // xi, yi - координаты, м
        //        double A =  0.5 * (x0 * (y1 - y2) + x1 * (y2 - y0) + x2 * (y0 - y1));
        //        if (std::isnan(A) || isNull(A))
        //        {
        //            std::cout <<"";
        //        }
        //        return A;

        return 0.5 * (x0 * (y1 - y2) + x1 * (y2 - y0) + x2 * (y0 - y1));
    }

    // Площадь четырёхугольника
    static inline double A_tetragon(std::pair<double,double> tl, std::pair<double,double> tr, std::pair<double,double> bl, std::pair<double,double> br)
    {
        // tl, tr, bl, br - координаты вершин четырёхугольника, м
        return 0.5 * ( (tl.first - br.first)*(tr.second - bl.second) + (bl.first - tr.first)*(tl.second - br.second) );
    }

    // Среднее ускоениие по треугольнику
    static inline double dudx_mean_for_tringle (double A, double z_0, double z_1, double z_2, double u_0, double u_1, double u_2)
    {
        // A - площадь треугольника, м2
        // xi_{0,2} - составляющие координат узлов, м
        // u_{0,2} - составляющие скоростей смещений, м/с
//        if (isNull(A))
//        {
//            std::cout << "dudx_mean_triangle is NULL!";
//        }
        if (isNull(A))
            return 0.0;
        return 0.5 * ((u_1 + u_0)*(z_1 - z_0) + (u_2 + u_1)*(z_2 - z_1) + (u_0 + u_2)*(z_0 - z_2)) / A;
    }

    // Среднее ускоениие по треугольнику
    static inline double dudz_mean_for_tringle (double A, double x_0, double x_1, double x_2, double u_0, double u_1, double u_2)
    {
        // A - площадь треугольника, м2
        // xi_{0,2} - составляющие координат узлов, м
        // u_{0,2} - составляющие скоростей смещений, м/с
//        if (isNull(A))
//        {
//            std::cout << "dudx_mean_triangle is NULL!";
//        }
        if (isNull(A))
            return 0.0;
        return 0.5 * ((u_1 + u_0)*(x_0 - x_1) + (u_2 + u_1)*(x_1 - x_2) + (u_0 + u_2)*(x_2 - x_0)) / A;
    }

    // Рассчёт условной массы треугольной зоны при условном шаге по времени dt=1
    // (см. документацию Itasca FLAC 8, Theory and Background: 1.3.5, формула (1.38))
    static inline double mass_triangle (double K, double G, double dx, double A)
    {
        // dx - максимальная дистанция внутри треугольника, м
        // A - площадь треугольника, м2
        if (isNull(A))
            return 0.0;
        return (K + 4./3. * G) * dx*dx / A;
    }


    // Рассчёт условной массы при условном шаге по времени dt=1
    // (см. документацию Itasca FLAC 8, Theory and Background: 1.3.5, формула (1.40))
    static inline double mass_nodal (double K, double G,
                                     double dx_bottomLeft, double dx_bottomRight, double dx_topLeft, double dx_topRight,
                                     double A_bottomLeft, double A_bottomRight, double A_topLeft, double A_topRight)
    {
        if (isNull(A_bottomLeft) || isNull(A_bottomRight) || isNull(A_topLeft) || isNull(A_topRight))
        {
            return 0.0;
        }
        return (K + 4./3. * G) / 6. * (
                    dx_bottomLeft*dx_bottomLeft / A_bottomLeft
                    +
                    dx_bottomRight*dx_bottomRight / A_bottomRight
                    +
                    dx_topLeft*dx_topLeft / A_topLeft
                    +
                    dx_topRight*dx_topRight / A_topRight
                    );
    }

    // Дэмпфирующая сила по координате Xi
    static inline double F_damp_xi (double F_node_xi, double u_xi)
    {
        // F_node_xi - составляющая узловой силы, Н
        // u_xi - составляющая скорости смещения, м/с

        // Дэмпфирующая сила противонаправлена мгновенному смещению
        // Если смещение нулевое, демпфирующая сила - тоже
        if (u_xi == 0)
            return 0;

        double ratio = 0.5;
        // singbit == TRUE если число отрицательное
        if (std::signbit(u_xi))
        {
            return ratio * fabs(F_node_xi);
        }
        return -ratio * fabs(F_node_xi);
    }

    // Ускорение смещения узла по координате Xi (u_xi -- скорость смещения узла по соответствующему направлению)
    // mass - условная, рассчитывается для сходимости, чтобы шаг по времени был одинаковый и любой
    // (см. документацию Itasca FLAC 8, Theory and Background: 1.3.5, формула (1.40))
    static inline double node_acceleration_xi (double F_node_xi, double mass)
    {
        if (mass == .0)
            return .0;
        return F_node_xi / mass;
    }

    // Координаты узла дополнительной сетки на ребре четырёхугольной ячейки главной сетки
    static inline std::pair<double,double> edge_center_pos (std::vector<std::vector<std::pair<double, double> > > mesh, int i1, int j1, int i2, int j2)
    {
        return std::pair<double, double> (0.5 * (mesh[i1][j1].first + mesh[i2][j2].first), 0.5 * (mesh[i1][j1].second + mesh[i2][j2].second));
    }

    // Координаты узла дополнительной сетки на пересечении диагоналей четырёхугольной ячейки главной сетки (находятся с помощью уравнений прямых по двум точкам)
    static inline std::pair<double,double> cell_ij_center_pos (std::vector<std::vector<std::pair<double, double> > > mesh, int i, int j)
    {
        if (i > (int)mesh.size()-2 || j > (int)mesh.front().size()-2)
        {
            double x = MY_BLANK, z = MY_BLANK;
            return std::pair<double,double> (x, z);
        }
        double k1 = (mesh[i+1][j+1].second - mesh[i][j].second) / (mesh[i+1][j+1].first - mesh[i][j].first);
        double l1 = (mesh[i][j].second * mesh[i+1][j+1].first - mesh[i+1][j+1].second * mesh[i][j].first)
                / (mesh[i+1][j+1].first - mesh[i][j].first);

        double k2 = (mesh[i+1][j].second - mesh[i][j+1].second) / (mesh[i+1][j].first - mesh[i][j+1].first);
        double l2 = (mesh[i][j+1].second * mesh[i+1][j].first - mesh[i+1][j].second * mesh[i][j+1].first)
                / (mesh[i+1][j].first - mesh[i][j+1].first);

        return std::pair<double, double> ((l2 - l1) / (k1 - k2), (k1*l2 - k2*l1) / (k1 - k2));
    }

    // Сила, приложенная к грани дополнительной сетки в данном треугольнике
    static inline std::pair<double,double> F_edge (double x_prev, double z_prev, double x_next, double z_next, double stressXX_T, double stressZZ_T, double stressXZ_T)
    {
        double n_xS = z_next - z_prev;
        double n_zS = - x_next + x_prev;
        double F_edge_x = stressXX_T * n_xS + stressXZ_T * n_zS;
        double F_edge_z = stressZZ_T * n_zS + stressXZ_T * n_xS;

        return std::pair<double, double> (F_edge_x, F_edge_z);
    }

};

#endif // FORMULAS_H
