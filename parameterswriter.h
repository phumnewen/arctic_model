#ifndef PARAMETERSWRITER_H
#define PARAMETERSWRITER_H

#include <memory>
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>

//

class ParametersWriter
{

public:

//    ParametersWriter();

    enum DataType
    {
        temp = 0,
        press = 1,
        dens = 2,
        stress = 3,
        force = 4,
        disp = 5,
        layers = 6
    };

    static bool writePolyLine(const std::string &fileName, std::vector<std::pair<double, double> > parameter);

    static bool writePolyLine(const std::string &fileName, std::vector<std::pair<int, double> > parameter);

    static bool writeScalar(const std::string &fileName, std::vector<std::vector<double> > parameter);

    static bool writeScalar(const std::string &fileName, std::vector<std::vector<int> > parameter);

    static bool writeVector(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > parameter);

    static bool writeTensor(const std::string &fileName, std::vector<std::vector<std::vector<double> > > parameter);

    static bool writeColormapScalar(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<double> > parameter, ParametersWriter::DataType dataType);

    static bool writeColormapScalar(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<int> > parameter, ParametersWriter::DataType dataType);

    static bool writeColormapVector(const std::string &fileName, std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<std::pair<double, double> > > parameter, ParametersWriter::DataType dataType);

};

#endif // PARAMETERSWRITER_H
