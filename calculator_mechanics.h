#ifndef CALCULATORMECHANICS_H
#define CALCULATORMECHANICS_H

#include <omp.h>
#include <iostream>

#include "formulas.h"
#include "model.h"
#include "modelwriter.h"

class CalculatorMechanics
{

public:

    CalculatorMechanics(std::shared_ptr<Model> _mdl);

//    void initializeModel(double Young, double v, double bulk, double shear);
//    void initializeModel(std::shared_ptr<std::vector<std::vector<int> > > _boundariesIndexesInMesh,
//                         std::vector<double> Youngs, std::vector<double> nus,
//                         std::vector<double> bulks, std::vector<double> shears,
//                         std::vector<double> densities,
//                         std::vector<double> stresses, std::pair<double, double> forces);
    // Вызывать только после инициализации параметров модели (initializeModel)
//    void setVelocityBoundaryConditions(std::pair<double, double> left, std::pair<double, double> right);
//    void setDisplacementBoundaryConditions(std::pair<double, double> left, std::pair<double, double> right);

//    void resetCell_dxMax (const std::vector<std::vector<std::pair<double, double> > > mesh, std::vector<std::vector<double> > *dxMaxInCell);
    void resetCell_dxMax ();
    double maxDisplacement();

    bool setStressAsLithostatic ();
    bool calculate (double totalMovemet = 0.0, int frequency = 0,
                    std::pair<double, double> leftVelBC = std::pair<double, double>(.0,.0),
                    std::pair<double, double> rightVelBC = std::pair<double, double>(.0,.0));
    bool runIsostate ();

    void interrupt() {interrupted = true;}


    enum BoundaryConditionsType{
        Extension = 0,
        Compression = 1,
        Sability = 2
    };

private:

//    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > mesh;

//    std::shared_ptr<std::vector<std::vector<double > > > E;
//    std::shared_ptr<std::vector<std::vector<double > > > nu;
//    std::shared_ptr<std::vector<std::vector<double > > > K;
//    std::shared_ptr<std::vector<std::vector<double > > > G;
//    std::shared_ptr<std::vector<std::vector<double > > > mass;

//    // Напряжения в треугольниках
//    //                   X           Z           T_i         s_ij
//    std::shared_ptr<std::vector<std::vector<std::vector<std::vector<double> > > > > stress;
////    std::shared_ptr<std::vector<std::vector<double> > > strainrate;
//    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > force;
//    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > displacement;
//    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > velocity_of_displacement;
//    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > gradients_of_velocity;
//    std::shared_ptr<std::vector<std::vector<double> > > dx_cell_max;

    bool interrupted = false;
//    double dxmax = 4;       // м
//    double timeLimit;       // с
//    unsigned layerNum = 4;  // воздух, осадочный чехол, кора, мантия
//    double maxForce;
//    double maxDisp;
//    double maxE;
//    double maxStr;
//    double minMass;

    // максимальное смещение
    std::vector<std::pair<int, double>> maxDispHist;


    std::shared_ptr<Model> mdl;
};

#endif // CALCULATORMECHANICS_H
