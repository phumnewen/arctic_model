#!/usr/bin/python3

from sys import argv
import numpy as np
import matplotlib.pyplot as P
from time import gmtime, strftime
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable

picDPI = 600

def readVectorFile(fileName, dataList_X, dataList_Y):
     fileToRead = open(fileName, 'r')
     
     subListMaxSize = 0
     
     for line in fileToRead.readlines():
          sub_list_x = []
          sub_list_y = []
          
          if line:
               for item in line[:-1].split('\t'):
                    if item:
                         sub_list_x.append (float(item.split(',')[0]))
                         sub_list_y.append (-float(item.split(',')[1]))
               
          dataList_X.append (sub_list_x)
          dataList_Y.append (sub_list_y)
  
def readTensorFile(fileName, dataList_X, dataList_Y):
     fileToRead = open(fileName, 'r')
     
     subListMaxSize = 0
     
     for line in fileToRead.readlines():
          sub_list_x = []
          sub_list_y = []
          
          if line:
               for item in line[:-1].split('\t'):
                    if item:
                         sub_list_x.append (float(item.split(',')[0]))
                         sub_list_y.append (-float(item.split(',')[1]))
               
          dataList_X.append (sub_list_x)
          dataList_Y.append (sub_list_y)
     
     
def readScalarIntFile(fileName, dataList):
     fileToRead = open(fileName, 'r')
     
     subListMaxSize = 0
     maxValue = -666666666
     
     for line in fileToRead.readlines():
          sub_list = []
     
          if line:
               for item in line[:-1].split('\t'):
                    if item:
                         val = int(item)
                         sub_list.append (val)
                         if val > maxValue:
                              maxValue = val
          dataList.append (sub_list)
     fileToRead.close()
     return maxValue
     
     
def readScalarFloatFile(fileName, dataList):
     fileToRead = open(fileName, 'r')

     subListMaxSize = 0
     minValue = 666666666.0
     maxValue = -666666666.0
     
     for line in fileToRead.readlines():
          sub_list = []
     
          if line:
               for item in line[:-1].split('\t'):
                    if item:
                         val = float(item)
                         sub_list.append (val)
                         if val < minValue:
                              minValue = val
                         if val > maxValue:
                              maxValue = val
          if subListMaxSize < len(sub_list):
               subListMaxSize = len(sub_list)
          dataList.append (sub_list)
          
     for subList in dataList:
          while len(subList) < subListMaxSize:
               subList.append(subList[-1])
          
     return [minValue, maxValue]


def readLineFile(fileName, dataList_X, dataList_Y):
     fileToRead = open(fileName, 'r')
     
     for line in fileToRead.readlines():
          item = line[:-2].split(',')
          if len(item) == 2:
               val1 = float(item[0])
               val2 = -float(item[1])
               dataList_X.append (val1)
               dataList_Y.append (val2)
     


def drawColorPlot(xData, yData, valuesData, valMin, valMax, colorMap, pictureName):
     fig, ax = P.subplots()
     ax.set_aspect('equal')
     q = ax.plot(xData, yData, 'ko', ms=0.1, mew=0.1)
     c = ax.pcolor(xData, yData, valuesData, cmap=colorMap, vmin=valMin, vmax=valMax)
     ax.plot(SRF_X, SRF_Y, color='k', linewidth = 0.4)
     # ~ ax.plot(SED_X, SED_Y, color='k', linewidth = 0.4)
     # ~ ax.plot(UCLC_X, UCLC_Y, color='k', linewidth = 0.4)
     # ~ ax.plot(MOHO_X, MOHO_Y, color='k', linewidth = 0.4)
     ax.plot(LAB_X, LAB_Y, color='k', linewidth = 0.4)
     divider = make_axes_locatable(ax)
     cax = divider.append_axes("right", size="5%", pad=0.2)
     P.colorbar(c, cax=cax)
     fig.savefig(prefix+"/"+pictureName+"_"+fileIdx, dpi=picDPI)
     
  
fileGroup = argv[1]

prefix = "/home/vit/Desktop/Arctic_model/model/output_1305"
markPrefix=""

fileIdx = fileGroup# + "_" + str(i)
meshFileName = prefix+"/"+markPrefix+"_mesh_"+ fileIdx+".csv"
cellsFileName = prefix+"/"+markPrefix+"_cells_"+ fileIdx+".csv"
layersFileName = prefix+"/"+markPrefix+"_layers_"+ fileIdx+".csv"
dataThermName = prefix+"/"+markPrefix+"_temperature_" + fileIdx+".csv"
dataPressName = prefix+"/"+markPrefix+"_pressure_" + fileIdx+".csv"
dataDensName = prefix+"/"+markPrefix+"_density_" + fileIdx+".csv"
# ~ dataMechName = prefix+"/force_" + fileIdx
dataMechName = prefix+"/"+markPrefix+"_displacement_" + fileIdx+".csv"
dataStressName = prefix+"/"+markPrefix+"_stress_" + fileIdx+".csv"
print ("Mesh file: ", meshFileName)
print ("Data file: ", dataMechName)


# ~ geom_curveSRF = "/data/vit/Arctic2012_data/Arctic2012_surf.csv"
# ~ geom_curveUCLC = "/data/vit/Arctic2012_data/Arctic2012_ulcb.csv"
# ~ geom_curveSED = "/data/vit/Arctic2012_data/Arctic2012_sediments.csv"
# ~ geom_curveMOHO = "/data/vit/Arctic2012_data/Arctic2012_moho.csv"
# ~ geom_curveLAB = "/data/vit/Arctic2012_data/Arctic2012_lab.csv"

# ~ geom_curveSRF = "/home/vit/Desktop/Arctic_model/test_data_flex/srf.csv"
# ~ geom_curveLAB = "/home/vit/Desktop/Arctic_model/test_data_flex/lab.csv"

# ~ geom_curveSRF = "/home/vit/Desktop/Arctic_model/Arctic2012_data/Arctic2012_surf.csv"
# ~ geom_curveSED = "/home/vit/Desktop/Arctic_model/Arctic2012_data/Arctic2012_sediments.csv"
# ~ geom_curveUCLC = "/home/vit/Desktop/Arctic_model/Arctic2012_data/Arctic2012_ulcb.csv"
# ~ geom_curveMOHO = "/home/vit/Desktop/Arctic_model/Arctic2012_data/Arctic2012_moho.csv"
# ~ geom_curveLAB = "/home/vit/Desktop/Arctic_model/Arctic2012_data/Arctic2012_lab.csv"

SRF_X = []
SRF_Y = []

SED_X = []
SED_Y = []

UCLC_X = []
UCLC_Y = []

MOHO_X = []
MOHO_Y = []

LAB_X = []
LAB_Y = []

# ~ readLineFile (geom_curveSRF, SRF_X, SRF_Y)
# ~ readLineFile (geom_curveSED, SED_X, SED_Y)
# ~ readLineFile (geom_curveUCLC, UCLC_X, UCLC_Y)
# ~ readLineFile (geom_curveMOHO, MOHO_X, MOHO_Y)
# ~ readLineFile (geom_curveLAB, LAB_X, LAB_Y)

    
## ----------------------------------------------------------------
## Reading mesh file
     
mesh_x = []
mesh_y = []

readVectorFile (meshFileName, mesh_x, mesh_y)

print ("Mesh size x:", len(mesh_x))
print ("Mesh size y:", len(mesh_y))

    
## ----------------------------------------------------------------
## Reading mesh file
     
cells_x = []
cells_y = []

readVectorFile (cellsFileName, cells_x, cells_y)

print ("Cells count x:", len(cells_x))
# ~ print (cells_x)
print ("Cells count y:", len(cells_y))
# ~ print (cells_y)


## ----------------------------------------------------------------
## Reading layers file
     
layers_idxs = []

layNum = 1 + readScalarIntFile(layersFileName, layers_idxs)

print ("Layers count:", layNum)
print ("Cells layers count:", len(layers_idxs))
# ~ print (layers_idxs)

# ~ drawColorPlot(cells_x, cells_y, layers_idxs, 0, layNum-0.5, 'gist_stern_r', "layers")
    

## ----------------------------------------------------------------
## Reading thermal data file
     
dataTherm = []

mmValsT = readScalarFloatFile(dataThermName, dataTherm)

minT = mmValsT[0]
maxT = mmValsT[1]

          
print ("Minimum temperture:", minT)
print ("Maximum temperture:", maxT)
print ("Temperature count:", len(dataTherm))

LAB_isotherm_X = []
LAB_isotherm_Y = []

# Looking for isotherm
added1 = False
added2 = False
for i in range(len(mesh_x)):
     for j in range(len(mesh_x[i])):
          if 1280 < dataTherm[i][j] < 1320:
               if not added1:# and not added2:
                    LAB_isotherm_X.append (mesh_x[i][j])
                    LAB_isotherm_Y.append (mesh_y[i][j])
                    added1 = True
                    # ~ added2 = True
               # ~ elif not added1 and added2:
                    # ~ added2 = False
               # ~ elif added1 and added2:
                    # ~ added1 = False
               else:
                    added1 = False

# ~ print (LAB_isotherm)
    

## ----------------------------------------------------------------
## Reading pressure data file
     
dataPress = []

mmValsP = readScalarFloatFile(dataPressName, dataPress)

minP = mmValsP[0]
maxP = mmValsP[1]

# ~ drawColorPlot(mesh_x, mesh_y, dataPress, minP, maxP, 'rainbow', "pressure")


## ----------------------------------------------------------------
## Reading density data file
     
dataDens = []

mmValsD = readScalarFloatFile(dataDensName, dataDens)

minD = mmValsD[0]
maxD = mmValsD[1]

# ~ drawColorPlot(mesh_x, mesh_y, dataDens, 0, 3500, 'rainbow', "density")

     
## ----------------------------------------------------------------
## Reading mechanic data file
     
dataMech_x = []
dataMech_y = []

readVectorFile (dataMechName, dataMech_x, dataMech_y)

print ("Mech data count x:", len(dataMech_x))
print ("Mech data count y:", len(dataMech_y))

     
## ----------------------------------------------------------------
## Reading stress data file
     
dataStress_x = []
dataStress_y = []

readVectorFile (dataMechName, dataMech_x, dataMech_y)

print ("Mech data count x:", len(dataMech_x))
print ("Mech data count y:", len(dataMech_y))

     
## ----------------------------------------------------------------
## Data arrows plot
     
# ~ fig, ax = P.subplots()
# ~ # ~ fig.settitle(fileIdx)
# ~ ax.set_aspect('equal')
# ~ q = ax.quiver(mesh_x, mesh_y, dataMech_x, dataMech_y, units='width', pivot='tail', width=0.001)#, angles='', scale=0.1, scale_units='xy'
# ~ # ~ ax.scatter(mesh_x, mesh_y, color='b', s=0.01)
# ~ ax.quiverkey(q, .92, -0.11, 10, r'$10\ \frac{kg\cdot m}{s^2}$', labelpos='E', coordinates='axes', color='g')
# ~ # ~ fig.savefig(prefix+"/force_"+fileIdx)
# ~ fig.savefig(prefix+"/displacement_"+fileIdx, dpi=picDPI)

# ~ fig, ax = P.subplots()
# ~ ax.set_aspect('equal')
# ~ c = ax.pcolor(mesh_x, mesh_y, dataTherm, cmap='rainbow', vmin=minT, vmax=1320)
# ~ ax.plot(LAB_isotherm_X, LAB_isotherm_Y, color='k', linewidth = 0.4)
# ~ divider = make_axes_locatable(ax)
# ~ cax = divider.append_axes("right", size="5%", pad=0.2)
# ~ P.colorbar(c, cax=cax)
# ~ fig.savefig(prefix+"/temperature_"+fileIdx, dpi=picDPI)  
     
     
# ~ P.show()




fig, ax = P.subplots()
ax.set_aspect('equal')
q = ax.plot(mesh_x, mesh_y, 'ko', ms=0.1, mew=0.1)#, angles='', scale=0.1, scale_units='xy'
# ~ ax.scatter(mesh_x, mesh_y, color='b', s=0.01)
# ~ ax.quiverkey(q, .92, -0.11, 10, r'$10\ \frac{kg\cdot m}{s^2}$', labelpos='E', coordinates='axes', color='g')
# ~ c = ax.pcolor(cells_x, cells_y, layers_idxs, cmap='gist_stern_r', vmin=0, vmax=layNum-0.5)
c = ax.pcolor(mesh_x, mesh_y, layers_idxs, cmap='gist_stern_r', vmin=0, vmax=layNum-0.5)
ax.plot(SRF_X, SRF_Y, color='k', linewidth = 0.4)
# ~ ax.plot(SED_X, SED_Y, color='k', linewidth = 0.4)
# ~ ax.plot(UCLC_X, UCLC_Y, color='k', linewidth = 0.4)
# ~ ax.plot(MOHO_X, MOHO_Y, color='k', linewidth = 0.4)
ax.plot(LAB_X, LAB_Y, color='k', linewidth = 0.4)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.2)
P.colorbar(c, cax=cax)


fig.savefig(prefix+"/laysss_"+fileIdx, dpi=picDPI)  

