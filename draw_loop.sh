#! /bin/bash

#~ data_path=/data/vit/output/
data_path=/home/vit/Desktop/Arctic_model/model/output_1305


echo $data_path


for i in `ls $data_path/_mesh* | sed -r "s/.+mesh_(.+).csv/\1/g" | uniq | sort -d` ; do
echo $i
/home/vit/Desktop/Arctic_model/model/elastic_model_2d/boundsMovement.py $i
/home/vit/Desktop/Arctic_model/model/elastic_model_2d/model_visualiser.py $i
#~ /data/vit/arctic_model/boundsMovement.py $i
done
