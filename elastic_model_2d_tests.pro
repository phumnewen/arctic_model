QT += core testlib
QT -= gui

CONFIG += c++11

TARGET = elastic_model_2d_tests
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += elastic_model_2d_tests/main.cpp \
    meshgenerator.cpp \
    elastic_model_2d_tests/tst_meshgenerator.cpp \
    modelgeometryreader.cpp \
    elastic_model_2d_tests/tst_modelgeometryreader.cpp

HEADERS += \
    meshgenerator.h \
    elastic_model_2d_tests/tst_meshgenerator.h \
    modelgeometryreader.h \
    elastic_model_2d_tests/tst_modelgeometryreader.h
