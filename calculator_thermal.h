#ifndef CALCULATOR_THERMAL_H
#define CALCULATOR_THERMAL_H

#include <omp.h>

#include "model.h"
#include "formulas.h"

class CalculatorThermal
{
public:
    CalculatorThermal();

    static bool calcPseudo2D(std::shared_ptr<Model> model);

    // i - индекс столбца в сетке
    static bool calc1D(std::shared_ptr<Model> model, unsigned i);

    static void smoothPTHoryzontally(std::shared_ptr<Model> model);

//    std::shared_ptr<Model> model;

};

#endif // CALCULATOR_THERMAL_H
