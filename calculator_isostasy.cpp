#include "calculator_isostasy.h"

CalculatorIsostasy::CalculatorIsostasy(std::shared_ptr<Model> _mdl):
    mdl(_mdl)
{

}

bool CalculatorIsostasy::runIsostate()
{

    // Столбец, в котором подошва литосферы ниже всего.
    int refLitCol = 0;
    // Глубиной изостатической компенсации (depthIC) выбирается глубина подошвы литосферы в её самом глубоком месте
    double depthIC = -666;
    // Индекс точки ИК в столбце
    int idxIC = 0;
    double pIC = 0.0;
    for (int i=1; i<(int)mdl->_mesh.size(); ++i)
    {
        for (int j=1; j<(int)mdl->_mesh[i].size(); ++j)
        {
            if (mdl->_layers.at(mdl->_cellLayerIndexes[i-1][j-1]) != "Astenosphere"
                    && mdl->_mesh[i][j].second > depthIC)
            {
                depthIC = mdl->_mesh[i].back().second;
                refLitCol = i;
                idxIC = (int)mdl->_mesh[i].size()-1;
            }
        }
    }
    // Давление на глубине ИК (предполагается, что термальная модель посчитана и давления в узлах известны)
    pIC = mdl->_P[refLitCol][idxIC];
    std::cout << "\nPressure IC " << pIC;
    std::cout << "\nDepth IC " << depthIC;


    // Считаем для всех остальных столбцов (кроме refLitCol)
    // давление на подошве литосферы, которое должно было бы быть при условии изостазии
    // так же считаем вес столба литосферы для каждого столба -- эти два давления не сойдутся
    // нужно будет что-то подогнать: либо плотность литосферы, либо её мощность
    // я подгоняю мощность

    // Необходимая точность равенства давлений в литосфере и астеносфере
    double prec = 1e2;

    for (int i=0; i<(int)mdl->_cells.size(); ++i)
    {
        if (i!=refLitCol)
        {
            // Индекс узла в колонке, который лежит на границе литосферы и астеносферы
            int labIdx = 0;

            if (i == 60)
                std::cout << "Some where here";


            for (int j=1; j<(int)mdl->_mesh[i].size(); ++j)
            {
                if (mdl->_layers.at(mdl->_cellLayerIndexes[i][j-1]) == "Lithosphere"
                        && mdl->_layers.at(mdl->_cellLayerIndexes[i][j]) == "Astenosphere")
                {
                    labIdx = j;
                }
            }

            double labDepth = std::min(mdl->_mesh[i][labIdx].second, mdl->_mesh[i+1][labIdx].second);
            double delZ = depthIC - labDepth;
            double Plab_t = mdl->_P[i][labIdx];
            double dDens = 0.5 * (mdl->_dens[i][idxIC-1] + mdl->_dens[i][labIdx]);
            double dPast = dDens * delZ * mdl->g;
            double Plab_ic = pIC - dPast;   // Стандартная плотность астеносферы

            while (!Formulas::areEqual(Plab_ic, Plab_t, prec))
            {
                double rat = Plab_ic / Plab_t;
                for (int j=0; j<labIdx; ++j)
                {
                    mdl->_dens[i][j] *= rat;
                }

                resetP(i);

                Plab_t = mdl->_P[i][labIdx];
            }
        }
    }

    // Считаем архимедову силу на границе литосферы/астеносферы в изостатическом случае
    for (int i=0; i<(int)mdl->_mesh.size(); ++i)
    {
        for (int j=1; j<(int)mdl->_mesh[i].size(); ++j)
        {
            mdl->_force_archimed[i][j].first = 0.0;
            mdl->_force_archimed[i][j].second = 0.0;

            if (i == 0 && j < (int)mdl->_mesh[0].size()-1
                    &&
                    mdl->_layers.at(mdl->_cellLayerIndexes[0][j-1]) == "Lithosphere" && mdl->_layers.at(mdl->_cellLayerIndexes[0][j]) == "Astenosphere")
            {
                mdl->_force_archimed[i][j].second -= mdl->_P[i][j] * (mdl->_mesh[i+1][j].first - mdl->_mesh[i][j].first);
            }
            else if (i > 0 && i < (int)mdl->_mesh.size()-1 && j < (int)mdl->_mesh[i-1].size()-1
                     &&
                     mdl->_layers.at(mdl->_cellLayerIndexes[i-1][j-1]) == "Lithosphere" && mdl->_layers.at(mdl->_cellLayerIndexes[i-1][j]) == "Astenosphere")
            {
                mdl->_force_archimed[i][j].second -= mdl->_P[i][j] * 0.5 * (mdl->_mesh[i+1][j].first - mdl->_mesh[i-1][j].first);
            }
            else if (i == (int)mdl->_mesh.size()-1 && j < (int)mdl->_mesh[i-1].size()-1
                     &&
                     mdl->_layers.at(mdl->_cellLayerIndexes[i-1][j-1]) == "Lithosphere" && mdl->_layers.at(mdl->_cellLayerIndexes[i-1][j]) == "Astenosphere")
            {
                mdl->_force_archimed[i][j].second -= mdl->_P[i][j] * (mdl->_mesh[i][j].first - mdl->_mesh[i-1][j].first);
            }
        }
    }

    return true;
}

void CalculatorIsostasy::resetP(int i)
{
    //  i -- индекс столбца ячеек

    for (int j=0; j<(int)mdl->_mesh[i].size(); ++j)
    {
        if (mdl->_layers.at(mdl->_cellLayerIndexes[i][j-1]) != "Air")
        {
            // Считаем давление для нижней границы из плотности
            mdl->_P[i][j] = mdl->_P[i][j-1] + (mdl->_mesh[i][j].second - mdl->_mesh[i][j-1].second)* mdl->g * mdl->_dens[i][j];
            if (i == (int)mdl->_cells.size()-1)
                mdl->_P[mdl->_cells.size()][j] = mdl->_P[mdl->_cells.size()][j-1]
                        + (mdl->_mesh[mdl->_cells.size()][j].second - mdl->_mesh[mdl->_cells.size()][j-1].second) * mdl->g * mdl->_dens.back()[j];
        }
        else
        {
            mdl->_P[i][j] = mdl->P_surf;
        }

    }
}
