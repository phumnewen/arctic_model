#ifndef MODELGEOMETRYREADER_H
#define MODELGEOMETRYREADER_H

#include <memory>
#include <vector>
#include <list>
#include <fstream>

class ModelGeometryReader
{

public:

//    ModelGeometryReader();

    static int read(const std::list<std::string> *fileNames,
                    std::vector<std::vector<std::pair<double, double> > > *frame,
                    const std::string delimiter=",",
                    const int linesToSkip=0);
};

#endif // MODELGEOMETRYREADER_H
