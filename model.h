#ifndef MODEL_H
#define MODEL_H

#include <memory>
#include <vector>
#include <map>
#include <cmath>

class Model
{
public:

    Model();
//    Model(const Model &otherModel);


    enum Sij
    {
        xx = 0,
        zz = 1,
        xz = 2
    };

    enum Triangle
    {
        T_0 = 0,
        T_1 = 1,
        T_2 = 2,
        T_3 = 3
    };


    // Инициализация модели
//    void initialize(double Young, double v, double bulk, double shear);
    // Очистка рассчётных параметров (заполнение нулями). Размерность векторов сохраняется
    void setParametersToZero();
    // Очистка физических свойств (заполнение нулями). Размерность векторов сохраняется
    void setPropertiesToZero();
    // Полная очистка рассчётных параметров
    void cleanParameters();
    // Полная очистка физических свойств
    void cleanProperties();
    // Придать всем векторам свойств и параметров размер, соответствующий сетке
    void fitPropertiesToMesh();
    // Сдвинуть границу между слоями вверх или вниз на одну ячейку (i, j - индексы ячейки над границей)
//    bool shiftBoundaryAtCell(int i, int j, bool up = true);

    // Граничные условия
    // Боковые
    void setLeftVelocity(double hor, double vert);
    void setRightVelocity(double hor, double vert);
    // Снизу (граница литосферы/астеносфера)
//    void addArchimedToForces();
    //private:

    //// Координаты узлов сетки

    // Базовые кривые для сетки, соответствующие геологическим границам
    std::vector<std::vector<std::pair<double, double> > > _frame;
    // Двумерный вектор сетки. Состоит из вертикально ориентированных подвекторов
    std::vector<std::vector<std::pair<double, double> > > _mesh;
    // Двумерный вектор сетки. Состоит из вертикально ориентированных подвекторов
    std::vector<std::vector<std::pair<double, double> > > _cells;
    // Для знания, какому слою принадлежит ячейка,
    // создаётся сетка точно как сетка ячеек, но её элементы -- не координаты центров ячеек, а индексы слоёв (целые числа, начиная с 0)
    std::vector<std::vector<int> > _cellLayerIndexes;
    // Хэш-таблица сопоставления названия слоя и его индекса
    std::map<int, std::string> _layers;
    // Хэш-таблица индекса слоя и его участия в рассчёте
    std::map<int, bool> _layerInCalculation;
    // Максимальное в ячейке расстояние между узлами
    std::vector<std::vector<double> > _dxCellMax;
    // Движение границ
    // Поверхности
    std::vector<std::pair<double, double> > srfInitial;
    std::vector<std::pair<double, double> > srfMovement;
    // Подошвы литосферы
    std::vector<std::pair<double, double> > labInitial;
    std::vector<std::pair<double, double> > labMovement;
    std::vector<std::pair<double, double> > lithMidline;


    //// Значения свойств модели

    ///Общие параметры модели
    // Ускорение силы тяжести, м/с2
    double g = 9.81;
    // Плотность, кг/м3
    std::vector<std::vector<double > > _dens; // В ячейках
    // Хэш-таблица стандартной плотности слоёв по индексу
    std::map<int, double> _dens_std; // Для слоя
    // Давление, Па
    std::vector<std::vector<double > > _P;    // В узлах
    // Температура, К
    std::vector<std::vector<double > > _T;    // В узлах

    /// Термальная модель
    // Температура на поверхности, K
    double T_surf = 273;
    // Давление на поверхности, Па
    double P_surf = 1.013e5;
    // Тепловой поток на поверхности, Вт/м2
    double q_surf = 50e-3;

    // Теплопроводность, Вт/(м*К)
    std::vector<std::vector<double > > _k;    // В ячейках
    // Коэффициенты для рассчёта теплопроводнсти по формулам из "T. Gerya. Introduction..."
    std::map<int, double> _k_addend;    // Для слоя
    std::map<int, double> _k_numerator;    // Для слоя
    // Теплопродуктивность, Вт/м3
    std::vector<std::vector<double > > _H;    // В ячейках
    // Теплоёмкость, Дж/К
    std::vector<std::vector<double > > _Cp;   // В ячейках
    // Термальное расширение, 1/К
    std::vector<std::vector<double > > _alfa; // В ячейках
    // Сжимаемость, 1/Па
    std::vector<std::vector<double > > _beta; // В ячейках


    /// Упругая модель
    // Скорости волн
    // Хэш-таблицы скоростей слоёв по индексу
    std::map<int, double> _vp;    // Для слоя
    std::map<int, double> _vs;    // Для слоя
    // Коэффициент Пуассона
    std::vector<std::vector<double > > _nu;   // В ячейках
    // Модуль Юнга, Па
    std::vector<std::vector<double > > _E;    // В ячейках
    // Модуль объёмной упругости, Па
    std::vector<std::vector<double > > _K;    // В ячейках
    // Коэффициент сдвига, Па
    std::vector<std::vector<double > > _G;    // В ячейках
    // Виртуальная масса для рассчёта механики, кг
    std::vector<std::vector<double > > _mass_v; // В узлах

    /// Рассчётные значения

    // Напряжения в треугольниках внутри ячеек
    //    X           Z           T_i         s_ij
    std::vector<std::vector<std::vector<std::vector<double> > > > _stress;    // В ячейках (в треугольниках)
    // Скорости деформаций -- центрированы в ячейках
    //    std::vector<std::vector<double> > _strainrate;
    // Силы смещения -- центрированы в узлах
    std::vector<std::vector<std::pair<double, double> > > _force; // В узлах
    // Сила тяжести
    std::vector<std::vector<std::pair<double, double> > > _gravity; // В узлах
    // Силы архимеда действуют на подошве литосферы -- центрированы в узлах
    std::vector<std::vector<std::pair<double, double> > > _force_archimed; // В узлах
    // Смещения -- центрированы в узлах
    std::vector<std::vector<std::pair<double, double> > > _displacement;  // В узлах
    // Скорости смещения -- центрированы в узлах
    std::vector<std::vector<std::pair<double, double> > > _velocity;  // В узлах
    // Градиенты скоростей смещения -- центрированы в узлах
    std::vector<std::vector<std::pair<double, double> > > _gradients_of_velocity; // В узлах

    double meshStepX;
    double meshStepZ;

    double maxDisplacement = 1.0;   // м
    double stationarCriterion = 1e-2; // м
};

#endif // MODEL_H
