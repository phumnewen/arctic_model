#include "meshgenerator.h"

MeshGenerator::MeshGenerator(std::shared_ptr<Model> _model):
    model(_model)
{
    // Задание шагов сетки по X и Z
    //    _model->meshStepX = 20;   // м
    //    _model->meshStepZ = 15;   // м
    // Задаём дополнительные мощности сверху и снизу
    topZeroThick = 2 * model->meshStepZ;  // м
    bottomAddTHick = 4 * model->meshStepZ;    // м
//    topZeroThick = 0;  // м
//    bottomAddTHick = 0;    // м
}

double MeshGenerator::valAtCurve (double x, const std::vector<std::pair<double, double> >& curve, double *distToCurve)
{
    if (curve.empty())
    {
        return Formulas::MY_BLANK;
    }
    // Экстраполяция
    if (x < curve.front().first)
    {
        if (nullptr != distToCurve)
        {
            *distToCurve = x - curve.front().first;
        }
        return curve.front().second;
    }
    else if (x > curve.back().first)
    {
        if (nullptr != distToCurve)
        {
            *distToCurve = x - curve.back().first;
        }
        return curve.back().second;
    }
    // Интерполяция
    else
    {
        //  Если интерпояция, то расстояние до кривой 0
        if (nullptr != distToCurve)
        {
            *distToCurve = 0;
        }
        int curveSize = (int)curve.size();
        for (int i=0; i<(int)curveSize-1; ++i)
        {
            // Случаи совпадения
            if (x == curve.at(i).first)
            {
                return curve.at(i).second;
            }
            else if (x == curve.at(i+1).first)
            {
                return curve.at(i+1).second;
            }
            else if ((x > curve.at(i).first && x < curve.at(i+1).first)
                     ||
                     (x < curve.at(i).first && x > curve.at(i+1).first))
            {
                double Dx = curve.at(i+1).first - curve.at(i).first;
                double Dy = curve.at(i+1).second - curve.at(i).second;
                double dx = x - curve.at(i).first;
                return (dx *Dy / Dx) + curve.at(i).second;
            }
        }
        // Возвращает бланкованное значение, если что-то пошло не так
        return Formulas::MY_BLANK;
    }
}

void MeshGenerator::meshRange()
{
    // км:
    xMin=1e5;
    xMax=-1e5;
    zMin=1e5;
    zMax = -1e5;
    for (const auto& curve: model->_frame)
    {
        for (const auto& point: curve)
        {
            if  (point.first < xMin)
            {
                xMin = point.first;
            }
            if (point.first > xMax)
            {
                xMax = point.first;
            }
            if  (point.second < zMin)
            {
                zMin = point.second;
            }
            if (point.second > zMax)
            {
                zMax = point.second;
            }
        }
    }
    // Прибавляем дополнительные мощности
    zMin -= topZeroThick;
    zMax +=bottomAddTHick;
}

void MeshGenerator::meshCreating()
{
    //Временный вектор индексов слоёв для ячеек
    std::vector<std::vector<int> > nodeLayerIndexesTmp;

    //#pragma omp parallel for
    for (double x=xMin; x<=xMax; x+=model->meshStepX)
    {
        std::vector<std::pair<double, double>> curvesCrossList;
        // Проходимся по всем кривым вектора геометрии модели и находим их пересечения с нашей колонкой
        for (const auto& frame_curve: model->_frame)
        {
            // Расстояние до кривой геометрии по X
            double xBoundaryDist;
            // Значение Z кривой геометрии
            double zBoundary = valAtCurve(x, frame_curve, &xBoundaryDist);
            curvesCrossList.push_back({ zBoundary, xBoundaryDist });
        }
        // Список будет отсортирован по координате z пересечения с нашей колонкой
        std::sort(curvesCrossList.begin(), curvesCrossList.end());

        // Вертикальный вектор точек на этой позиции по X
        std::vector<std::pair<double, double>> vMesh;
        // Для знания, какому слою принадлежит узел,
        // создаётся сетка точно как основная, но её элементы -- не координаты узлов, а индексы слоёв (целые числа, начиная с 0)
        // Вектор, который содержит индексы принадлежности к слою для каждого узла на этой позиции по X
        std::vector<int> vLayerIndexes;
        int layerIndex = 0;

        //#pragma omp parallel for
        for (double z=zMin; z<=zMax; z+=model->meshStepZ)
        {
            // Адаптированное значение Z для узла
            double zAdapted = Formulas::MY_BLANK;
            // Проходимся по всем кривым вектора геометрии модели (они условно горизонталльны)
            for (int i=0; i<(int)curvesCrossList.size(); ++i)
            {
                // Значение Z кривой геометрии
                double zBoundary = curvesCrossList[i].first;
                // Расстояние до кривой геометрии по X
                double xBoundaryDist = curvesCrossList[i].second;
                // Если на этой глубине в этой точке по X
                // расстояние до текущей кривой геометрии меньше,
                // чем половина шага по Z...
                if (fabs(zBoundary-z) < model->meshStepZ/2)
                {
                    // ...то если ещё и кривая задана на этом значении по X
                    if (fabs(xBoundaryDist) < model->meshStepX)
                    {
                        // то перезаписываем Z для будущего узла сетки, делая его равным Z кривой геометрии
                        zAdapted = zBoundary;
                        // Поскольку совпадение по высоте нашлось, то счётчик слоев должен быть увеличен
                        layerIndex++;

                        if (i<(int)curvesCrossList.size()-1)
                        {
                            for (int j=i+1; j<(int)curvesCrossList.size(); ++j)
                            {
                                if (fabs(curvesCrossList[j].second) >= model->meshStepX)
                                {
                                    layerIndex++;
                                }
                            }
                        }
                    }
                    else if (i==0)
                    {
                        // Поскольку совпадение по высоте нашлось, то счётчик слоев должен быть увеличен
                        layerIndex++;

                        for (int j=i+1; j<(int)curvesCrossList.size(); ++j)
                        {
                            if (fabs(curvesCrossList[j].second) >= model->meshStepX)
                            {
                                layerIndex++;
                            }
                        }
                    }
                }
            }
            // zAdapted был объявлен как BLANK
            // Если бы вблизи текущего положения по Z имелась хоть одна кривая геометрии,
            // он был бы переписан
            if (!Formulas::isBlank(zAdapted))
            {
                vMesh.push_back(std::pair<double, double> (x, zAdapted));
                zAdapted = Formulas::MY_BLANK;
            }
            // Если вблизи текущего положения по Z кривых геометрии,
            // то узел будет иметь текущую координату по Z
            else
            {
                vMesh.push_back(std::pair<double, double> (x, z));
            }
            vLayerIndexes.push_back(layerIndex);
        }
        // Запись вертикального вектора в сетку
        model->_mesh.push_back(vMesh);
        nodeLayerIndexesTmp.push_back(vLayerIndexes);
    }

    // Заполняем вектор координат центров ячеек
    model->_cells.resize(model->_mesh.size()-1);
    for (int i=0; i<(int)model->_cells.size(); ++i)
    {
        std::vector<int> vLayerIndexes;
        model->_cells[i].resize(model->_mesh[i].size()-1);
        for (int j=0; j<(int)model->_cells[i].size(); ++j)
        {
            model->_cells[i][j] = Formulas::cell_ij_center_pos(model->_mesh, i, j);
            // Заполняем вектор индексов слоёв в ячейках
            //            if (!model->_layerInCalculation.at(nodeLayerIndexesTmp[i][j]) || !model->_layerInCalculation.at(nodeLayerIndexesTmp[i+1][j]))
            //                vLayerIndexes.push_back(std::max(nodeLayerIndexesTmp[i][j], nodeLayerIndexesTmp[i+1][j]));
            //            if (!model->_layerInCalculation.at(nodeLayerIndexesTmp[i][j+1]) || !model->_layerInCalculation.at(nodeLayerIndexesTmp[i+1][j+1]))
            //                vLayerIndexes.push_back(std::max(nodeLayerIndexesTmp[i][j+1], nodeLayerIndexesTmp[i+1][j+1]));
            //            else
            //                vLayerIndexes.push_back(std::max(nodeLayerIndexesTmp[i][j], nodeLayerIndexesTmp[i+1][j]));
            vLayerIndexes.push_back(std::max(nodeLayerIndexesTmp[i][j], nodeLayerIndexesTmp[i+1][j]));
        }
        model->_cellLayerIndexes.push_back(vLayerIndexes);
    }
    // Торчащие углы ячеек, где граница слоёв переходит в другой слой ячеек подсглаживаем
    for (int i=0; i<(int)model->_cells.size()-1; ++i)
    {
        for (int j=0; j<(int)model->_cells[i].size()-1; ++j)
        {
            int tl_idx = model->_cellLayerIndexes[i][j];
            int tr_idx = model->_cellLayerIndexes[i+1][j];
            int bl_idx = model->_cellLayerIndexes[i][j+1];
            int br_idx = model->_cellLayerIndexes[i+1][j+1];
            if (((tl_idx == tr_idx) && (tr_idx == bl_idx)  && (bl_idx != br_idx))
                    || ((tl_idx == tr_idx) && (tr_idx == br_idx) && (br_idx != bl_idx)))
            {
                double z_c = model->_mesh[i+1][j+1].second;
                double z_bb = model->_mesh[i+1][j+2].second;
                // Приближаем торчащий угол к границе слоёв
                model->_mesh[i+1][j+2].second = model->_mesh[i+1][j+2].second - (z_bb - z_c)/4;
//                std::cout << "\nBound corrected.";
            }

        }
    }
}

bool MeshGenerator::generateMesh()
{
    /// Определение начальной и конечной X-координат разметки сетки
    // Вычисляем минимальнуюи максимальную по X точки сетки
    meshRange();

    std::cout << "\nX mesh range: " << xMin << " " << xMax;
    std::cout << "\nZ mesh range: " << zMin << " " << zMax << "\n";

    std::cout << "\nStart generating...\n";
    /// Собственно разметка сетки от xMin до xMax в соответсвии с model->meshStepX и mediateCurvesCount
    meshCreating();

    // Натягиваем все свойства на созданную сетку
    //    model->fitPropertiesToMesh();

    return true;
}
