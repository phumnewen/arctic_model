#include "modelwriter.h"
#include "modelsetupreader.h"
#include "meshgenerator.h"
#include "calculator_mechanics.h"
#include "calculator_thermal.h"
#include "calculator_isostasy.h"


// Аргументы:
// 1. char* delimiter -- разделитель (опционально)
// 2. int linesToSkip -- Количество строк в заголовке, которые пропускаются при чтении (опционально)
// 3. собственно сами файлы кривых


std::list<std::string> parseArgs(int argc, char *argv[]);
void initialParameters(std::pair<double, double> *leftVelocityBoundaryCondition,
                       std::pair<double, double> *rightVelocityBoundaryCondition,
                       std::vector<double> *youngs,
                       std::vector<double> *nus,
                       std::vector<double> *bulks,
                       std::vector<double> *shears,
                       std::vector<double> *dens,
                       std::vector<double> *stresses,
                       std::pair<double,double> *forces);


// --------------------------------------------------------
// --------------------------------------------------------


int main(int argc, char *argv[])
{
    if (argc < 1)
    {
        std::cout << "\nPlease, use the SetUp file path as an argument!\n";
        return 0;
    }

    // Аргумент для запуска программы - путь к файлу Setup
    std::string setupFile = argv[1];


    // Объект модели содержит все параметры и рассчётные значения на сетке
    std::shared_ptr<Model> model = std::make_shared<Model> ();

    // Пути к файлам кривых геометрии
    std::list<std::string> geomFileNames;
    // Разделитель в файлах геометрии
    std::string delimiter;
    // Путь к файлу физических свойств для слоёв
    std::string layPhysPropFile;
    // Количество строк заголовка, которые будут пропущены при чтении
    int linesToSkip;

    /// Чтение файла модели

    std::cout << "\nModel Setup reading...\n";

    int modelReadResult = ModelSetupReader::readSetup(setupFile, model, geomFileNames, delimiter, linesToSkip, layPhysPropFile);
    if (modelReadResult < 0)
    {
        // Ошибка чтения
        std::cout << "\nModel Setup reading failed!\nRun interrupted. Code: " << modelReadResult << "\n";
        return -1;
    }
    else
    {
        std::cout << "\nModel Setup read successfully!\n";
    }

    std::cout << "\nModel geometry reading...\n";

    /// Чтение кривых геометрии

    int geomReadCode = ModelGeometryReader::read(&geomFileNames, &model->_frame, delimiter, linesToSkip);
    if (geomReadCode < 0)
    {
        // Ошибка чтения
        std::cout << "\nModel geometry reading failed!\nRun interrupted. Code: " << geomReadCode << "\n";
        return -1;
    }
    else
    {
        std::cout << "\nModel geometry read successfully!\n";
    }

    /// Построение сетки

    std::cout << "\nMesh generation...\n";
    MeshGenerator meshGen(model);
    if (meshGen.generateMesh())
    {
        model->fitPropertiesToMesh();
        std::cout << "\nMesh generated successfully.\n";
    }
    else
    {
        std::cout << "\nFailed to generate mesh!\n";
        return -1;
    }

    /// Инициализаци физических свойств из файла
    if (!layPhysPropFile.empty())
    {

        if (int code = ModelSetupReader::readPhysProp(layPhysPropFile, model) != 0)
        {
            std::cout << "\nError during reading physical properties! Code: " << code << "\n";
        }
        else
        {
            std::cout << "\nOutput data is written succesfully.\n";
        }
    }

//    std::time_t timeStartTherm = std::time(nullptr);
//    char timeStr[100];
//    std::strftime(timeStr, sizeof(timeStr), "%Y-%m-%d_%H:%M", std::gmtime(&timeStartTherm));
//    std::string UTCTimeStartTherm (timeStr);
//    std::cout << "\n" << UTCTimeStartTherm << "\n";


    /// Термальный рассчёт

    std::cout << "\nStart thermal simulation...\n" << std::endl;

    // Инициализируем термальный калькулятор
    if (CalculatorThermal::calcPseudo2D(model))
    {
        std::cout << "\nThermal simulation finished successfully.\n";
    }
    else
    {
        std::cout << "\nError during the thermal simulation!\n";
    }

    if (int code = ModelWriter::writeModel(model) > 0)
    {
        std::cout << "\nError during the output data writing! Code: " << code << "\n";
    }
    else
    {
        std::cout << "\nOutput data is written succesfully.\n";
    }


//    std::time_t timeStartMech = std::time(nullptr);
//    char timeStr[100];
//    std::strftime(timeStr, sizeof(timeStr), "%Y-%m-%d_%H:%M", std::gmtime(&timeStartMech));
//    std::string UTCTimeStartMech (timeStr);
//    std::cout << "\n" << UTCTimeStartMech << "\n";


//    /// Рассчёт изостазии

    std::cout << "\nStart isostatic simulation...\n" << std::endl;

    CalculatorIsostasy calcIsosate(model);
    if (calcIsosate.runIsostate())
    {
        std::cout << "\nIsostatic simulation finished successfully.\n";
    }
    else
    {
        std::cout << "\nError during the isostatic simulation!\n";
    }

    if (int code = ModelWriter::writeModel(model, "isostatic") > 0)
    {
        std::cout << "\nError during the output data writing! Code: " << code << "\n";
    }
    else
    {
        std::cout << "\nOutput data is written succesfully.\n";
    }


    /// Механический рассчёт

    std::cout << "\nStart mechanic simulation...\n" << std::endl;

    // Создаём механический калькулятор
    CalculatorMechanics calcMech(model);

    // ------
    // Задаём начальное напряженное состояние -- литостотическое
    // ------

    std::cout << "\nSetting lithostatic setress..." << std::endl;

    if (calcMech.setStressAsLithostatic())
    {
        std::cout << "\nLithostatic stress set successfully.\n";
    }
    else
    {
        std::cout << "\nLithostatic stress setting error!\n";
    }

    if (int code = ModelWriter::writeModel(model, "lithostatic") > 0)
    {
        std::cout << "\nError during the output data writing! " << code << "\n";
    }
    else
    {
        std::cout << "\nOutput data is written succesfully.\n";
    }

    // ------
    // Устаканиваем модель после рассчёта изостазии
    // ------

    std::cout << "\nModel equilibration..." << std::endl;

    if (calcMech.calculate())
    {
        std::cout << "\nModel equilibred successfully.\n";
    }
    else
    {
        std::cout << "\nEquilibration run into an error!\n";
    }

    if (int code = ModelWriter::writeModel(model, "equilibred") > 0)
    {
        std::cout << "\nError during the output data writing! " << code << "\n";
    }
    else
    {
        std::cout << "\nOutput data is written succesfully.\n";
    }

    std::cout << "\nExtension simulation..." << std::endl;

    // ------
    // Моделирование растяжения/сжатия
    // ------

    // Общее растяжение, которого нужно достигнуть, м
    double totalExtension = 1e4;
    // Растяжение за один акт, м
    double unitExtension = 100;
    // Частота ввода растяжения (раз во сколько итераций по вермени)
    int extensionFrequency = 20;

    if (calcMech.calculate(totalExtension, extensionFrequency, std::pair<double,double> (-unitExtension, 0.0)))
    {
        std::cout << "\nExtension simulation finished successfully.\n";
    }
    else
    {
        std::cout << "\nExtension simulation run into an error!\n";
    }

    /// Вывод данных

    std::cout << "\nOutput data writing down...\n";
    if (int code = ModelWriter::writeModel(model) > 0)
    {
        std::cout << "\nError during the output data writing! " << code << "\n";
    }
    else
    {
        std::cout << "\nOutput data is written succesfully.\n";
    }



    //    std::string scrCmd = "/home/vit/Desktop/Arctic_model/forces_viualiser.py " + UTCtime;
    //    system(scrCmd.c_str());

    return 0;
}


// --------------------------------------------------------
// --------------------------------------------------------

