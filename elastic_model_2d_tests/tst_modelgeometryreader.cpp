#include "tst_modelgeometryreader.h"

TestModelGeometryReader::TestModelGeometryReader(QObject *parent) : QObject(parent)
{

}

void TestModelGeometryReader::read()
{
    // Заполняем правильный вектор сетки (то, что должно получиться после считывания)
    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > correctMesh_V1;
    std::vector<std::pair<double, double> > top_line {
        std::pair<double, double> (-0.0870252, 0.612910),
                std::pair<double, double> (0.0277176, 6.72178),
                std::pair<double, double> (1.33656, 17.8990),
                std::pair<double, double> (2.55919, 30.0965),
                std::pair<double, double> (4.04372, 40.0479),
                std::pair<double, double> (4.36104, 51.6527),
                std::pair<double, double> (4.69793, 68.1461),
                std::pair<double, double> (4.39793, 83.4303),
                std::pair<double, double> (4.55425, 99.9274),
                std::pair<double, double> (3.42292, 110.543),
                std::pair<double, double> (1.57094, 121.581),
                std::pair<double, double> (-0.284309, 131.804),
                std::pair<double, double> (-2.22740, 142.640),
                std::pair<double, double> (-4.62110, 153.689),
                std::pair<double, double> (-5.58408, 161.246),
                std::pair<double, double> (-6.09890, 167.979),
                std::pair<double, double> (-5.07966, 174.477),
                std::pair<double, double> (-3.79039, 180.765),
                std::pair<double, double> (-1.68283, 188.463),
                std::pair<double, double> (0.865564, 193.504),
                std::pair<double, double> (2.42488, 199.583)
    };

    std::vector<std::pair<double, double> > middle_line {
        std::pair<double, double> (1.98894,22.9784),
                std::pair<double, double> (3.25947,24.5822),
                std::pair<double, double> (4.80901,28.2173),
                std::pair<double, double> (6.27315,33.0765),
                std::pair<double, double> (7.46807,38.3485),
                std::pair<double, double> (8.67032,45.4538),
                std::pair<double, double> (9.51143,52.5664),
                std::pair<double, double> (10.7202,61.3011),
                std::pair<double, double> (11.9355,71.6654),
                std::pair<double, double> (12.3513,85.3053),
                std::pair<double, double> (11.9463,96.9249),
                std::pair<double, double> (9.81775,106.542),
                std::pair<double, double> (6.77981,114.549),
                std::pair<double, double> (3.36688,119.100),
                std::pair<double, double> (1.47821,120.972)
    };

    std::vector<std::pair<double, double> > bottom_line {
        std::pair<double, double> (29.7115,1.02443),
                std::pair<double, double> (29.5717,11.2126),
                std::pair<double, double> (28.9113,26.7078),
                std::pair<double, double> (28.9513,36.6886),
                std::pair<double, double> (28.6464,50.7506),
                std::pair<double, double> (28.3456,65.8311),
                std::pair<double, double> (28.0301,77.2451),
                std::pair<double, double> (28.6199,89.2518),
                std::pair<double, double> (30.0060,97.1681),
                std::pair<double, double> (33.1295,110.549),
                std::pair<double, double> (34.3415,120.099),
                std::pair<double, double> (35.9237,131.881),
                std::pair<double, double> (37.8539,140.398),
                std::pair<double, double> (38.9814,151.375),
                std::pair<double, double> (39.9356,164.189),
                std::pair<double, double> (39.9837,176.207),
                std::pair<double, double> (38.8556,187.637),
                std::pair<double, double> (37.7096,194.587),
                std::pair<double, double> (36.1032,199.305)
    };

    std::vector<std::vector<std::pair<double, double> > > curves {top_line, middle_line, bottom_line};

    // Список тестовых файлов кривых (набор тестовых данных версии 1)
    std::list<std::string> fileNames_V1 = {
        "../../test_input_data/top_line.csv",
        "../../test_input_data/bottom_line.csv",
        "../../test_input_data/middle_line.csv"
    };
    std::string delimiter_V1 = ",";
    int headerLineCount = 6;

    std::shared_ptr<std::vector<std::vector<std::pair<double, double> > > > testMesh;
    testMesh = std::make_shared<std::vector<std::vector<std::pair<double, double> > > > (curves);
    ModelGeometryReader reader;
    reader.read(fileNames_V1, testMesh, delimiter_V1, headerLineCount);
    QCOMPARE (testMesh, correctMesh_V1);
}
