#ifndef TST_MODELGEOMETRYREADER_H
#define TST_MODELGEOMETRYREADER_H

#include <QObject>
#include <QtTest>
#include "../modelgeometryreader.h"

class TestModelGeometryReader : public QObject
{
    Q_OBJECT

public:

    explicit TestModelGeometryReader(QObject *parent = nullptr);


public slots:

    void read();


};

#endif // TST_MODELGEOMETRYREADER_H
