#ifndef TST_MESHGENERATOR_H
#define TST_MESHGENERATOR_H

#include <QObject>
#include <QtTest>
#include "../meshgenerator.h"

class TestMeshGenerator : public QObject
{
    Q_OBJECT

public:

    explicit TestMeshGenerator(QObject *parent = nullptr);


private slots:

    void valAtCurve();
    void meshRange();

};

#endif // TST_MESHGENERATOR_H
