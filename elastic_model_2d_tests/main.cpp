#include <QCoreApplication>
#include <QtTest>

#include <iostream>

#include "tst_meshgenerator.h"
#include "tst_modelgeometryreader.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv); //если нужно тестировать классы интерфейса, меняем QCoreApplication на QApplication
    Q_UNUSED(a);
    int result = 0;

    // Инициализация классов тестов
    TestModelGeometryReader readerTest;
    TestMeshGenerator meshTests;

    //собственно запуск тестов - эта функция последовательно выполнит все описанные в классе тестов тестовые кейсы
    result += QTest::qExec(&readerTest, argc, argv);
    result += QTest::qExec(&meshTests, argc, argv);


    // ------------------------------------------------------------------------
    // Итоговое сообщение о результатах чтобы не просматривать простыню отчетов
    // ------------------------------------------------------------------------

    std::cout << "\n*** RESULT REPORT ***\n";

    if (result == 0)
        std::cout << "\n All tests passed successfully!\n" << std::endl;
    else
        std::cout << " \n !!! Some tests failed!\n" << std::endl;

    return result;
}
