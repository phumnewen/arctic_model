#include "tst_meshgenerator.h"

TestMeshGenerator::TestMeshGenerator(QObject *parent) : QObject(parent)
{

}

void TestMeshGenerator::valAtCurve()
{
    MeshGenerator meshGenerator;

    // Дистация по X до кривой
    double distToCurve = mylib::MY_BLANK;

    std::vector<std::pair<double, double>> test_curve;

    // На пустой кривой должно вернуть бланк
    QCOMPARE (meshGenerator.valAtCurve(0, test_curve, &distToCurve), mylib::MY_BLANK);
    // Дистанция не вычисляется
    QCOMPARE (distToCurve, mylib::MY_BLANK);

    // Заполняем кривую
    std::pair<double, double> p1(10., 5.);
    std::pair<double, double> p2(73.2, 13.5);
    std::pair<double, double> p3(186., 144.1);
    std::pair<double, double> p4(235.7, 92.3);
    std::pair<double, double> p5(329.3, 156.2);
    test_curve.push_back(p1);
    test_curve.push_back(p2);
    test_curve.push_back(p3);
    test_curve.push_back(p4);
    test_curve.push_back(p5);

    // X < X первой точки кривой
    QCOMPARE (meshGenerator.valAtCurve(.0, test_curve, &distToCurve), test_curve.front().second);
    QCOMPARE (distToCurve, .0-10.);
    // X внутри кривой посередине отрезка
    double x1 = (73.2 + 10.) / 2;
    double y1 = (13.5 + 5.) / 2;
    QCOMPARE (meshGenerator.valAtCurve(x1, test_curve, &distToCurve), y1);
    QCOMPARE (distToCurve, .0);
    // X внутри кривой в произвольном месте отрезка отрезка
    double x2 = 200.;
    double y2 = (x2 - 186.) / (235.7 -186.) * (92.3 - 144.1) + 144.1;
    QCOMPARE (meshGenerator.valAtCurve(x2, test_curve, &distToCurve), y2);
    QCOMPARE (distToCurve, .0);
    // X внутри кривой в произвольном месте отрезка отрезка
    double x3 = 303.3;
    double y3 = (x3 - 235.7) / (329.3 - 235.7) * (156.2 - 92.3) + 92.3;
    QCOMPARE (meshGenerator.valAtCurve(x3, test_curve, &distToCurve), y3);
    QCOMPARE (distToCurve, .0);
    // X > X последней точки кривой
    QCOMPARE (meshGenerator.valAtCurve(400.2, test_curve, &distToCurve), test_curve.back().second);
    double xDist = 400.2 - 329.3;
    QCOMPARE (distToCurve, xDist);
}

void TestMeshGenerator::meshRange()
{
    MeshGenerator meshGenerator;

}

