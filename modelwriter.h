#ifndef MODELWRITER_H
#define MODELWRITER_H

#include <ctime>

#include "model.h"
#include "parameterswriter.h"


class ModelWriter
{
public:

//    ModelWriter();

    enum boundIdx
    {
        srf = 0,
        lab = 1
    };

    static int writeModel (std::shared_ptr<Model> model, std::string note = "");

    static std::vector<std::vector<std::vector<double>>> stressInCells (std::shared_ptr<Model> model);
    static std::vector<std::pair<double, double>> boundaryLine (std::shared_ptr<Model> model, ModelWriter::boundIdx bound);
    static std::vector<std::pair<double, double>> boundaryLineMovement (std::shared_ptr<Model> model, ModelWriter::boundIdx bound);
    static std::vector<std::pair<double, double>> getLithMidLine (std::shared_ptr<Model> mdl);

};

#endif // MODELWRITER_H
