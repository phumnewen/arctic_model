#!/usr/bin/python3

from sys import argv
import matplotlib.pyplot as P
from time import gmtime, strftime
import math
from mpl_toolkits.axes_grid1 import make_axes_locatable

picDPI = 600


def readLineFile(fileName, dataList_X, dataList_Y):
     fileToRead = open(fileName, 'r')
     
     for line in fileToRead.readlines():
          item = line[:-1].split('\t')
          # ~ print(item)
          if len(item) == 2:
               val1 = float(item[0])
               # Minus because positive movement is downward 
               val2 = -float(item[1])
               dataList_X.append (val1)
               dataList_Y.append (val2)


fileGroup = argv[1]
fileIdx = fileGroup

# ~ prefix = "/data/vit"
prefix = "/home/vit/Desktop/Arctic_model/model/output_1305"
dataSurfLineName = prefix+"/_surfLine_" + fileIdx+".csv"
dataLABLineName = prefix+"/_labLine_" + fileIdx+".csv"
dataMidLineName = prefix+"/_midLine_" + fileIdx+".csv"


## ----------------------------------------------------------------
## Reading boundaries data file

dataSurf_x = []
dataSurf_y = []
dataLAB_x = []
dataLAB_y = []
dataMidLine_x = []
dataMidLine_y = []

readLineFile(dataSurfLineName, dataSurf_x, dataSurf_y)
# ~ readLineFile(dataLABLineName, dataLAB_x, dataLAB_y)
# ~ readLineFile(dataMidLineName, dataMidLine_x, dataMidLine_y)

# ~ print(dataLAB_x, dataLAB_y)

fig, ax = P.subplots()
# ~ ax.set_aspect('equal')
# ~ q = ax.plot(dataSurf_x, dataSurf_y, 'ko', ms=1, mew=1)

# ~ P.xlim([-2, 12])
# ~ P.ylim([-87500, -85500])
# ~ P.xlim([150000, 350000])
# ~ q = ax.plot(dataLAB_x, dataLAB_y, 'ro', ms=1, mew=1)
# ~ fig.savefig(prefix+"/labMV_"+fileIdx, dpi=picDPI)  
q = ax.plot(dataSurf_x, dataSurf_y, 'ro', ms=1, mew=1)
fig.savefig(prefix+"/srfMV_"+fileIdx, dpi=picDPI)  
# ~ q = ax.plot(dataMidLine_x, dataMidLine_y, 'ro', ms=1, mew=1)
# ~ fig.savefig(prefix+"/midLine_"+fileIdx, dpi=picDPI)  

# ~ P.show()
