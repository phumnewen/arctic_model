#ifndef MODELSETUPREADER_H
#define MODELSETUPREADER_H

#include <fstream>
#include <sstream>

#include "model.h"
#include "formulas.h"
#include "modelgeometryreader.h"


class ModelSetupReader
{
public:
//    ModelSetupReader();

    static int readSetup(const std::string& fileName, std::shared_ptr<Model> model, std::list<std::string>& geomFileNames, std::string& delimiter, int& linesToSkip, std::string& layPhysPropFile);
    static int readPhysProp(const std::string& fileName, std::shared_ptr<Model> model);
};

#endif // MODELSETUPREADER_H
