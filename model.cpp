#include "model.h"

Model::Model()
{

}

//Model::Model(const Model &otherModel)
//{
//    _frame = otherModel._frame;
//    _mesh = otherModel._mesh;
//    _cells = otherModel._cells;
//    _cellLayerIndexes = otherModel._cellLayerIndexes;

//    _vp = otherModel._vp;
//    _vs = otherModel._vs;
//    _nu = otherModel._nu;
//    _E = otherModel._E;
//    _K = otherModel._K;
//    _G = otherModel._G;
//    _dens = otherModel._dens;
//    _dens_std = otherModel._dens_std;
//    _P = otherModel._P;
//    _T = otherModel._T;

//    _stress = otherModel._stress;
//    _force = otherModel._force;
//    _gravity = otherModel._gravity;
//    _force_archimed = otherModel._force_archimed;
//    _displacement = otherModel._displacement;
//    _velocity = otherModel._velocity;
//    _gradients_of_velocity = otherModel._gradients_of_velocity;
//}

//void Model::initialize(double Young, double v, double bulk, double shear)
//{
//    // Очистка векторов параметров
//    _E.clear();
//    _nu.clear();
//    _K.clear();
//    _G.clear();
//    _E_std.clear();
//    _K_std.clear();
//    _G_std.clear();
//    _mass_v.clear();
//    _stress.clear();
//    _force.clear();
//    _displacement.clear();
//    _velocity_of_displacement.clear();
//    _gradients_of_velocity.clear();
//    //    if (dx_cell_max)
//    //        dx_cell_max.clear();
//    for (int i=0; i<_mesh.size(); ++i)
//    {
//        int zsize = _mesh[i].size();   // Размер всех подвекторов одинаков
//        std::vector<double> E_vct;
//        std::vector<double> nu_vct;
//        std::vector<double> K_vct;
//        std::vector<double> G_vct;
//        std::vector<double> mass_vct;
//        std::vector<std::pair<double,double>> F_vct;
//        std::vector<std::pair<double,double>> disp_vct;
//        std::vector<std::pair<double,double>> vel_vct;
//        std::vector<std::pair<double,double>> acc_vct;
//        std::vector <double> str_for_trangle(3, 0.0);
//        std::vector <std::vector<double>>strs_in_cell(4, str_for_trangle);
//        std::vector<std::vector<std::vector<double>>> stress_vct(zsize-1, strs_in_cell);
//        for (int j=0; j<zsize; ++j)
//        {
//            E_vct.push_back(Young);
//            nu_vct.push_back(v);
//            K_vct.push_back(bulk);
//            G_vct.push_back(shear);
//            mass_vct.push_back(0.0);
//            F_vct.push_back(std::pair<double, double>(0.0, 0.0));
//            disp_vct.push_back(std::pair<double, double>(0.0, 0.0));
//            vel_vct.push_back(std::pair<double, double>(0.0, 0.0));
//            acc_vct.push_back(std::pair<double, double>(0.0, 0.0));
//        }
//        _nu.push_back(nu_vct);
//        _E.push_back(E_vct);
//        _K.push_back(K_vct);
//        _G.push_back(G_vct);
//        _E_std.push_back(E_vct);
//        _K_std.push_back(K_vct);
//        _G_std.push_back(G_vct);
//        _mass_v.push_back(mass_vct);
//        _force.push_back(F_vct);
//        _displacement.push_back(disp_vct);
//        _velocity_of_displacement.push_back(vel_vct);
//        _gradients_of_velocity.push_back(acc_vct);
//        if (i<_mesh.size()-1)
//        {
//            _stress.push_back(stress_vct);
//        }
//    }
//    // Заполнение dxMax для ячеек сетки
//    //    std::vector <double> zVct(zsize-1, 0.0);
//    //    std::vector <std::vector<double>> xVct(xsize-1, std::vector <double> (zsize-1, 0.0));
//    //    dx_cell_max = std::make_shared<std::vector<std::vector<double> > > (xVct);
//    //    resetCell_dxMax();
//}

void Model::cleanParameters()
{
    _stress.clear();
    _force.clear();
    _gravity.clear();
    _force_archimed.clear();
    _displacement.clear();
    _velocity.clear();
    _gradients_of_velocity.clear();
    _dxCellMax.clear();
}

void Model::cleanProperties()
{
    _P.clear();
    _T.clear();
    _dens.clear();
    _dens_std.clear();

    _k.clear();
    _k_addend.clear();
    _k_numerator.clear();
    _Cp.clear();
    _H.clear();
    _alfa.clear();
    _beta.clear();

    _vp.clear();
    _vs.clear();
    _nu.clear();
    _E.clear();
    _K.clear();
    _G.clear();
    _mass_v.clear();
}

void Model::fitPropertiesToMesh()
{
    cleanParameters();
    cleanProperties();

    std::pair<double, double> point (0.0, 0.0);
    std::vector<double> tensor (3, 0.0);
    // Вектор четырёх тензоров для каждого треугольника (по двум разбиениям) ячейки
    std::vector<std::vector<double>> cellTriangleTensors (4, tensor);

    // Параметры на узлах
    for  (int i=0; i<(int)_mesh.size(); ++i)
    {
        int zNodeCount = (int)_mesh[i].size();

        // Вектор по Z для векторных параметров, заданных на узлах
        std::vector<double> nodesParamScalZ (zNodeCount, 0.0);
        // Вектор по Z для векторных параметров, заданных на узлах
        std::vector<std::pair<double, double>> nodesParamVctZ (zNodeCount, point);

        _P.push_back(nodesParamScalZ);
        _T.push_back(nodesParamScalZ);

        _force.push_back(nodesParamVctZ);
        _gravity.push_back(nodesParamVctZ);
        _force_archimed.push_back(nodesParamVctZ);
        _displacement.push_back(nodesParamVctZ);
        _velocity.push_back(nodesParamVctZ);
        _gradients_of_velocity.push_back(nodesParamVctZ);
        _mass_v.push_back(nodesParamScalZ);
    }

    // Параметры в ячейках
    for  (int i=0; i<(int)_cells.size(); ++i)
    {
        int zCellCount = (int)_cells[i].size();

        // Вектор по Z для векторных параметров, заданных в ячейках
        std::vector<double> cellsParamScalZ (zCellCount, 0.0);
        // Вектор по Z для векторных параметров, заданных в ячейках
        std::vector<std::vector<std::vector<double>>> cellsParamTriTensZ (zCellCount, cellTriangleTensors);

        _dxCellMax.push_back(cellsParamScalZ);
        _dens.push_back(cellsParamScalZ);
        _k.push_back(cellsParamScalZ);
        _Cp.push_back(cellsParamScalZ);
        _H.push_back(cellsParamScalZ);
        _alfa.push_back(cellsParamScalZ);
        _beta.push_back(cellsParamScalZ);

        _nu.push_back(cellsParamScalZ);
        _E.push_back(cellsParamScalZ);
        _K.push_back(cellsParamScalZ);
        _G.push_back(cellsParamScalZ);
        _stress.push_back(cellsParamTriTensZ);
    }
}

//bool Model::shiftBoundaryAtCell(int i, int j, bool up)
//{
//    // !!! Требует пересчёт 1D термальной модели !!!
//    // Для пересчёта после выполнения функции вызовите CalculatorThermal::calc1D(modelName, i);


//    // Двигает нижнюю границу ячейки

//    // i, j - индексы границы в векторе узлов
//    // или индексы ячейки над границей в векторе ячеек
//    if (i<0 || i>=_cells.size() || j<=0 || j>=_cells[i].size()-1)
//    {
//        return false;
//    }

//    if (up)
//    {
//        _cellLayerIndexes[i][j] = _cellLayerIndexes[i][j+1];
////        _dens[i][j] = _dens[i][j+1];
////        _k[i][j] = _k[i][j+1];
//        _Cp[i][j] = _Cp[i][j+1];
//        _H[i][j] = _H[i][j+1];
//        _alfa[i][j] = _alfa[i][j+1];
//        _beta[i][j] = _beta[i][j+1];

//        _nu[i][j] = _nu[i][j+1];
//        _E[i][j] = _E[i][j+1];
//        _K[i][j] = _K[i][j+1];
//        _G[i][j] = _G[i][j+1];
//    }

//    else
//    {
//        _cellLayerIndexes[i][j] = _cellLayerIndexes[i][j-1];
////        _dens[i][j] = _dens[i][j-1];
////        _k[i][j] = _k[i][j-1];
//        _Cp[i][j] = _Cp[i][j-1];
//        _H[i][j] = _H[i][j-1];
//        _alfa[i][j] = _alfa[i][j-1];
//        _beta[i][j] = _beta[i][j-1];

//        _nu[i][j] = _nu[i][j-1];
//        _E[i][j] = _E[i][j-1];
//        _K[i][j] = _K[i][j-1];
//        _G[i][j] = _G[i][j-1];
//    }

//    return true;
//}

void Model::setLeftVelocity(double hor, double vert)
{
    for (int j=0; j<(int)_velocity.front().size(); j++)
    {
        _velocity.front()[j].first = hor;
        _velocity.front()[j].second = vert;
    }
}

void Model::setRightVelocity(double hor, double vert)
{
    for (int j=0; j<(int)_velocity.back().size(); j++)
    {
        _velocity.back()[j].first = hor;
        _velocity.back()[j].second = vert;
    }
}

//void Model::addArchimedToForces()
//{
//    for (int i=0; i<_force.size(); ++i)
//    {
//        for (int j=0; j<_force[i].size(); ++j)
//        {
//            _force[i][j].first += _force_archimed[i][j].first;
//            _force[i][j].second += _force_archimed[i][j].second;
//        }
//    }
//}

void Model::setPropertiesToZero()
{
    //    _E = otherModel._E;
    //    _nu = otherModel._nu;
    //    _K = otherModel._K;
    //    _G = otherModel._G;
    //    _dens = otherModel._dens;
    //    _P = otherModel._P;
    //    _T = otherModel._T;
}

