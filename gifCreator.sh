#! /bin/bash

pngPath="/home/vit/Desktop/Arctic_model/model/output/movement"
#_mv-4km_cell_6x3km"
#~ pngPath="/data/vit/model/output"


## --------
## PROFILES


CNTR=0
lastTime="2019-04-27_03:42:14"

#~ for i in `ls "$pngPath"/laysss*` ; do
	#~ IDtime=$(echo $i | sed -r "s/.+laysss_(.+).png/\1/")
	#~ echo "$i"
	
	#~ mv "$pngPath"/laysss_"$IDtime".png "$pngPath"/laysss_"$CNTR".png 
	#~ mv "$pngPath"/bounds_"$IDtime".png "$pngPath"/bounds_"$CNTR".png 
	#~ mv "$pngPath"/displacement_"$IDtime".png "$pngPath"/displacement_"$CNTR".png 

	#~ CNTR=$(($CNTR+1))
#~ done

#~ for i in `ls "$pngPath"/laysss*` ; do
	#~ number=$(echo $i | sed -r "s/.+laysss_([0-9]{3}).png/\1/")

	#~ mv "$pngPath"/laysss_"$number".png "$pngPath"/laysss_"$CNTR".png 
	#~ mv "$pngPath"/bounds_"$number".png "$pngPath"/bounds_"$CNTR".png 
	#~ mv "$pngPath"/displacement_"$number".png "$pngPath"/displacement_"$CNTR".png 
	#~ CNTR=$(($CNTR+1))
#~ done

#~ for i in `ls "$pngPath"/laysss*` ; do
	#~ number=$(echo $i | sed -r "s/.+laysss_([0-9]{1}).png/\1/")

	#~ mv "$pngPath"/laysss_"$number".png "$pngPath"/laysss_00"$number".png 
	#~ mv "$pngPath"/bounds_"$number".png "$pngPath"/bounds_00"$number".png 
	#~ mv "$pngPath"/displacement_"$number".png "$pngPath"/displacement_00"$number".png 
#~ done

#~ for i in `ls "$pngPath"/laysss*` ; do
	#~ number=$(echo $i | sed -r "s/.+laysss_([0-9]{2}).png/\1/")

	#~ mv "$pngPath"/laysss_"$number".png "$pngPath"/laysss_0"$number".png 
	#~ mv "$pngPath"/bounds_"$number".png "$pngPath"/bounds_0"$number".png 
	#~ mv "$pngPath"/displacement_"$number".png "$pngPath"/displacement_0"$number".png 
#~ done

#~ ffmpeg -r 2 -y -i "$pngPath"/laysss_%03d.png "$pngPath"/profile_"$IDtime".gif
ffmpeg -r 2 -y -i "$pngPath"/bounds_%03d.png "$pngPath"/boundMovement_"$lastTime".gif
ffmpeg -r 2 -y -i "$pngPath"/displacement_%03d.png "$pngPath"/displacement_"$lastTime".gif
