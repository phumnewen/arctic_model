#include "calculator_thermal.h"

CalculatorThermal::CalculatorThermal()
//model(_model)
{

}

bool CalculatorThermal::calcPseudo2D(std::shared_ptr<Model> model)
{
    bool ok = true;
    // Стационарное решение
    for (int i=0; i<(int)model->_mesh.size(); ++i)
    {
        if (calc1D(model, i) == false)
        {
            ok = false;
        }
    }

    smoothPTHoryzontally(model);
    smoothPTHoryzontally(model);

    return ok;
}

void CalculatorThermal::smoothPTHoryzontally(std::shared_ptr<Model> model)
{
    for(int i=1; i<(int)model->_mesh.size()-1; ++i)
    {
        for (int j=0; j<(int)model->_mesh[i].size(); ++j)
        {
            model->_P[i][j] =(model->_P[i-1][j] + model->_P[i][j] + model->_P[i+1][j]) / 3;
            model->_T[i][j] =(model->_T[i-1][j] + model->_T[i][j] + model->_T[i+1][j]) / 3;
        }
    }
}

bool CalculatorThermal::calc1D(std::shared_ptr<Model> model, unsigned i)
{
    //  i - индекс колонки в векторе model->_mesh

    model->_T[i].front() = model->T_surf;
    model->_P[i].front() = model->P_surf;
    double q = model->q_surf;   // Heat flux in the cell

    for (int j=1; j<(int)model->_mesh[i].size(); ++j)
    {
        bool isAir = true;
        if (i>0)
        {
            if (model->_layers.at(model->_cellLayerIndexes[i-1][j-1]) != "Air")
                isAir = false;
            else
                isAir = true;
        }
        else
        {
            if (model->_layers.at(model->_cellLayerIndexes[0][j-1]) != "Air")
                isAir = false;
            else
                isAir = true;
        }
        if (!isAir)
        {
            double H, dens, k_addend, k_numerator, alfa, beta, D = 3e4;
            std::string layer;
            double dz = model->_mesh[i][j].second - model->_mesh[i][j-1].second;

            if (i>0)
            {
                layer = model->_layers.at(model->_cellLayerIndexes[i-1][j-1]);
                H = model->_H[i-1][j-1];
                k_addend = model->_k_addend.at(model->_cellLayerIndexes[i-1][j-1]);
                k_numerator = model->_k_numerator.at(model->_cellLayerIndexes[i-1][j-1]);
                alfa = model->_alfa[i-1][j-1];
                beta = model->_beta[i-1][j-1];
            }
            else
            {
                layer = model->_layers.at(model->_cellLayerIndexes[0][j-1]);
                H = model->_H[0][j-1];
                k_addend = model->_k_addend.at(model->_cellLayerIndexes[0][j-1]);
                k_numerator = model->_k_numerator.at(model->_cellLayerIndexes[0][j-1]);
                alfa = model->_alfa[0][j-1];
                beta = model->_beta[0][j-1];
            }
            bool isMantle = (layer=="Lithosphere" || layer=="Astenosphere");
            if (!isMantle)
            {
                H *= exp(-model->_mesh[i][j].second/D);
            }
            q = q - dz * H;

            //            for (int cc=0; cc<5; ++cc)
            //            {
            if (i>0)
            {
                // Считаем теплопроводность исходя из температуры и давления на верхней границы ячейки
                model->_k[i-1][j-1] = Formulas::lambda(model->_T[i][j-1], model->_P[i][j-1], k_addend, k_numerator, isMantle);
                // Считаем температуру на нижней границе ячейки
                if(layer == "Astenosphere")
                {
                    // Для конвектирующей мантии увеличение температуры с глубиной K/m (адиабата)
                    model->_T[i][j] = model->_T[i][j-1] + dz * 5e-3;
                }
                else
                {
                    model->_T[i][j] = model->_T[i][j-1] + q * dz / model->_k[i-1][j-1];
                }
                // Считаем плотность исходя из температуры и давления на верхней границы ячейки
                dens = model->_dens_std.at(model->_cellLayerIndexes[i-1][j-1]) * exp(beta * (model->_P[i][j-1] - model->P_surf) - alfa * (model->_T[i][j-1] - model->T_surf));
                if (model->_layers.at(model->_cellLayerIndexes[i-1][j-1]) != "Asthenosphere")
                {
                    dens = model->_dens_std.at(model->_cellLayerIndexes[i-1][j-1]);
                }
                model->_dens[i-1][j-1] = dens;
                double v = model->_nu[i-1][j-1] = Formulas::nu(model->_vp.at(model->_cellLayerIndexes[i-1][j-1]), model->_vs.at(model->_cellLayerIndexes[i-1][j-1]));
                double E = model->_E[i-1][j-1] = Formulas::E(dens, model->_vp.at(model->_cellLayerIndexes[i-1][j-1]), model->_vs.at(model->_cellLayerIndexes[i-1][j-1]));
                model->_K[i-1][j-1] = Formulas::K(v, E);
                model->_G[i-1][j-1] = Formulas::G(v, E);
            }
            else if (i==0)
            {
                // Считаем теплопроводность исходя из температуры и давления на верхней границы ячейки
                model->_k[0][j-1] = Formulas::lambda(model->_T[i][j-1], model->_P[i][j-1], k_addend, k_numerator, isMantle);
                // Считаем температуру на нижней границе ячейки
                if(layer == "Astenosphere")
                {
                    // Для конвектирующей мантии увеличение температуры с глубиной 0.005 K/m (адиабата)
                    model->_T[i][j] = model->_T[i][j-1] + dz * 5e-3;
                }
                else
                {
                    model->_T[i][j] = model->_T[i][j-1] + q * dz / model->_k[0][j-1];
                }
                // Считаем плотность исходя из температуры и давления на верхней границы ячейки
                dens = model->_dens_std.at
                        (model->_cellLayerIndexes[0][j-1]) * exp(beta * (model->_P[i][j-1] - model->P_surf) - alfa * (model->_T[i][j-1] - model->T_surf));
                model->_dens[0][j-1] = dens;
                double v = model->_nu[0][j-1] = Formulas::nu(model->_vp.at(model->_cellLayerIndexes[0][j-1]), model->_vs.at(model->_cellLayerIndexes[0][j-1]));
                double E = model->_E[0][j-1] = Formulas::E(dens, model->_vp.at(model->_cellLayerIndexes[0][j-1]), model->_vs.at(model->_cellLayerIndexes[0][j-1]));
                model->_K[0][j-1] = Formulas::K(v, E);
                model->_G[0][j-1] = Formulas::G(v, E);
            }
            // Считаем давление для нижней границы из плотности
            model->_P[i][j] = model->_P[i][j-1] + dz * model->g * dens;
            //            }
        }
        else
        {
            model->_T[i][j] = model->T_surf;
            model->_P[i][j] = model->P_surf;
        }
    }
    return true;
}
