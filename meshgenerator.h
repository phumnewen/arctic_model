#ifndef MESHGENERATOR_H
#define MESHGENERATOR_H

#include <memory>
#include <iostream>
#include<algorithm>
#include <omp.h>

#include "model.h"
#include "formulas.h"


class MeshGenerator
{

public:
    MeshGenerator(std::shared_ptr<Model> _model);

    bool generateMesh();
//    inline const std::vector<std::vector<std::pair<double, double> > > *mesh() const { return &_frame; }

    double valAtCurve (double x, const std::vector<std::pair<double, double> >& curve, double *distToCurve = nullptr);


private:

    // Диапазон сетки по X и Z
    double xMin, xMax, zMin, zMax;    // км
    // Шаг по X и по Z (базовый шаг:
    // шаг по Z вблизи геологическох границ меняется при адаптации сетки
//    double xStep, zStep; // км
    // Дополнительная мощность нулевого материала сверху
    double topZeroThick;
    // Дополнительная мощность мантийного/нулевого материала снизу
    double bottomAddTHick;

    // Нахождение минимальной и максимальной по X точек сетки
    void meshRange ();
    // Собственно создание сетки
    void meshCreating ();

    std::shared_ptr<Model> model;
};

#endif // MESHGENERATOR_H
