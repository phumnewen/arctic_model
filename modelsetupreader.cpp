#include "modelsetupreader.h"

int ModelSetupReader::readSetup(const std::string& fileName, std::shared_ptr<Model> model, std::list<std::string>& geomFileNames, std::string& delimiter, int& linesToSkip, std::string& layPhysPropFile)
{

    if (!model)
    {
        return -1;
    }
    // Предполагается, что сетка уже сгенерирована и соответсвует файлу свойств
    // На всякий случай зануляем все значения свойств
    model->setPropertiesToZero();

    setlocale(LC_ALL, "en_US.UTF-8");
    //    int b_size = 50;
    //    char buf[b_size];
    std::ifstream inStream(fileName);
    if (!inStream.is_open())
    {
        inStream.close();
        return -2;
    }

    std::cout << "\n";

    // Дальше чтение данных
    try
    {
        std::string line;
        while(getline(inStream, line))
        {
            // Пустые строки и комментарии пропускаются
            if (line.empty() || line.front() == '#')
            {
                continue;
            }

            // Парсим строку по пробелам, удаляя пустые места
            std::vector<std::string> lineSplitted;
            std::stringstream lineStream(line); // Turn the string into a stream.
            std::string item;

            // Берём подстроку item из строки lineStream по разделителю пробел
            while(getline(lineStream, item, ' '))
            {
                // Следим, чтобы не было пустых элементов и пробелов, если идёт несколько пробелов подряд
                if (!item.empty() && item != " " && item != "\t")
                    lineSplitted.push_back(item);
            }

            // Читаем файлы кривых геометрии
            if (lineSplitted.size() == 2 && lineSplitted.front() == "geom_curve:")
            {
               geomFileNames.push_back(lineSplitted.back());
            }
            // Читаем слои модели
            if (lineSplitted.size() == 3)
            {
                model->_layers.insert(std::make_pair(std::stoi(lineSplitted.at(1)), lineSplitted.at(0)));
                model->_layerInCalculation.insert(std::make_pair(std::stoi(lineSplitted.at(1)), (bool)std::stoi(lineSplitted.at(2))));
            }
            //Параметры парсинга файлов кривых геометрии
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "lines_to_skip:")
            {
               linesToSkip = std::stoi(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "delimiter:")
            {
               delimiter = lineSplitted.back();
            }
            // Частота сетки
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "meshStepX=")
            {
               model->meshStepX = std::stod(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "meshStepZ=")
            {
               model->meshStepZ = std::stod(lineSplitted.back());
            }

            // Общие физические параметры модели
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "g=")
            {
                model->g = std::stod(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "T_surf=")
            {
                model->T_surf = std::stod(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "P_surf=")
            {
                model->P_surf = std::stod(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "q_surf=")
            {
                model->q_surf = std::stod(lineSplitted.back());
            }
            else if (lineSplitted.size() == 2 && lineSplitted.front() == "stationarity_criterion=")
            {
                model->stationarCriterion = std::stod(lineSplitted.back());
            }

            // Файл физических свойств для разных типов пород (они же - слои в модели)

            else if (lineSplitted.size() == 2 && lineSplitted.front() == "physPropFile:")
            {
                layPhysPropFile = lineSplitted.back();
            }

        }
    }
    catch (...)
    {
        inStream.close();
        // Вылет с исключением
        return -4;
    }

    inStream.close();
    return 0;
}

int ModelSetupReader::readPhysProp(const std::string& fileName, std::shared_ptr<Model> model)
{

    if (!model)
    {
        return -1;
    }
    // Предполагается, что сетка уже сгенерирована и соответсвует файлу свойств
    // На всякий случай зануляем все значения свойств
    model->setPropertiesToZero();

    setlocale(LC_ALL, "en_US.UTF-8");
    //    int b_size = 50;
    //    char buf[b_size];
    std::ifstream inStream(fileName);
    if (!inStream.is_open())
    {
        inStream.close();
        return -2;
    }

    std::cout << "\n";


    try
    {
        // Дальше чтение данных
        std::string line;
        while(getline(inStream, line))
        {
            // Пустые строки и комментарии пропускаются
            if (line.empty() || line.front() == '#')
            {
                continue;
            }

            // Парсим строку по пробелам, удаляя пустые места
            std::vector<std::string> lineSplitted;
            std::stringstream lineStream(line); // Turn the string into a stream.
            std::string item;

            // Берём подстроку item из строки lineStream по разделителю пробел
            while(getline(lineStream, item, '\t'))
            {
                lineSplitted.push_back(item);
            }
            // Всего задаётся 9 параметров + имя слоя и его индекс - итого 11 элементов в строке
            if (lineSplitted.size() != 11)
            {
                return -3;
            }
            // Выводим название слоя, помещаем его в хэш-таблицу с ключом-индексом и удаляем из списка
            std::cout << "Reading properties for layer № " << lineSplitted.at(1) << ": " << lineSplitted.at(0) << "\n";
//            model->_layers.insert(std::make_pair(std::stoi(lineSplitted.at(1)), lineSplitted.at(0)));
            lineSplitted.erase(lineSplitted.begin());

            // Берём индекс слоя и удаляем его из списка
            int layIdx = std::stoi(lineSplitted.front());
            lineSplitted.erase(lineSplitted.begin());


            // Плотность
            model->_dens_std.insert(std::make_pair(layIdx, std::stod(lineSplitted.at(0))));
            model->_k_addend.insert(std::make_pair(layIdx, std::stod(lineSplitted.at(1))));
            model->_k_numerator.insert(std::make_pair(layIdx, std::stod(lineSplitted.at(2))));
            model->_vp.insert(std::make_pair(layIdx, std::stod(lineSplitted.at(7))));
            model->_vs.insert(std::make_pair(layIdx, std::stod(lineSplitted.at(8))));

            for (unsigned i=0; i<model->_cells.size(); ++i)
            {
                for (unsigned j=0; j<model->_cells[i].size(); ++j)
                {
                    if (model->_cellLayerIndexes[i][j] == layIdx)
                    {
//                        // Слагаемое для формулы рассчёта коэффициента теплопроводности
//                        if (i < model->_k_addend.size() && j < model->_k_addend[i].size()) // Чтоб не падало уж
//                        {
//                            model->_k_addend[i][j] = std::stod(lineSplitted.at(1));
//                        }
//                        // Числитель для формулы рассчёта коэффициента теплопроводности
//                        if (i < model->_k_addend.size() && j < model->_k_addend[i].size()) // Чтоб не падало уж
//                        {
//                            model->_k_numerator[i][j] = std::stod(lineSplitted.at(2));
//                        }
                        // Теплоёмкость
                        if (i < model->_Cp.size() && j < model->_Cp[i].size()) // Чтоб не падало уж
                        {
                            model->_Cp[i][j] = std::stod(lineSplitted.at(3));
                        }
                        // Коэффициент теплового расширения
                        if (i < model->_alfa.size() && j < model->_alfa[i].size()) // Чтоб не падало уж
                        {
                            model->_alfa[i][j] = std::stod(lineSplitted.at(4));
                        }
                        // Сжимаемость
                        if (i < model->_beta.size() && j < model->_beta[i].size()) // Чтоб не падало уж
                        {
                            model->_beta[i][j] = std::stod(lineSplitted.at(5));
                        }
                        // Теплопродукция
                        if (i < model->_H.size() && j < model->_H[i].size()) // Чтоб не падало уж
                        {
                            model->_H[i][j] = std::stod(lineSplitted.at(6));
                        }
                        //
//                        if (i < model->_E_std.size() && j < model->_E_std[i].size()
//                                &&
//                                i < model->_nu.size() && j < model->_nu[i].size()
//                                &&
//                                i < model->_K_std.size() && j < model->_K_std[i].size()
//                                &&
//                                i < model->_G_std.size() && j < model->_G_std[i].size()
//                                &&
//                                i < model->_dens_std.size() && j < model->_dens_std[i].size()
//                                ) // Чтоб не падало уж
//                        {
//                            double Vp = std::stod(lineSplitted.at(7));
//                            double Vs = std::stod(lineSplitted.at(8));
//                            double E = model->_E_std[i][j] = Formulas::E(model->_dens_std[i][j], Vp, Vs);
//                            double nu = model->_nu[i][j] = Formulas::nu(Vp, Vs);
//                            model->_K_std[i][j] = Formulas::K(nu, E);
//                            model->_G_std[i][j] = Formulas::G(nu, E);
//                        }
                    }
                }
            }
        }
    }
    catch (...)
    {
        inStream.close();
        // Вылет с исключением
        return -4;
    }


    inStream.close();
    return 0;
}
