#include "modelgeometryreader.h"

//ModelGeometryReader::ModelGeometryReader()
//{

//}

int ModelGeometryReader::read(const std::list<std::string> *fileNames,
                              std::vector<std::vector<std::pair<double, double> > > *frame,
                              const std::string delimiter,
                              const int linesToSkip)
{
    // Промежуточный вектор кривых, несортированный
//    std::vector<std::vector<std::pair<double, double> > > mesh_tmp;

    // Список средних значений Z для всех кривых
    // нужен для сортировки кривых в итоговом векторе mesh -- там они должны идти по возрастанию средней глубины
//    std::vector <double> curvesZAvg;
    for (std::string fName: *fileNames)
    {
        setlocale(LC_ALL, "en_US.UTF-8");
        int b_size = 50;
        char buf[b_size];
        std::ifstream inStream(fName);
        if (!inStream.is_open())
        {
            inStream.close();
            return -1;
        }

        // Одна кривая из одного файла
        std::vector<std::pair<double, double> > curve;
        curve.clear();

        try
        {
            // Прокручиваем строки заголовка, которые не должны быть прочитаны
            int i=0;
            while (i<linesToSkip)
            {
                inStream.getline(buf, b_size);
                ++i;
            }

            // Счётчик количества точек текущей кривой
            int pc = 0;
            // Суммарное значение z для текущей кривой
//            double zSum = 0;
            // Дальше чтение данных
            // Читаем первую строку
            std::string line;
            while(getline(inStream, line))
            {
                // Пустые строки пропускаются
                if (line.empty())
                {
                    continue;
                }
                // Парсим строку по пробелам, удаляя пустые места
                std::string x_str = line.substr(0, line.find(delimiter));
                std::string z_str = line.substr(line.find(delimiter)+1, line.find(delimiter));

//                double x = std::stod(x_str);
//                double z = std::stod(z_str);
                double x = 1000 * std::stod(x_str);
                double z = 1000 * std::stod(z_str);
                // Добавление точки
                ++pc;
//                zSum += z;
                curve.push_back(std::pair<double, double>(x, z));
            }
//            curvesZAvg.push_back(zSum / pc);
            inStream.close();
        }
        catch (...)
        {
            inStream.close();
            // Вылет с исключением
            return -2;
        }

        frame->push_back(curve);
    }

    return 0;
}
